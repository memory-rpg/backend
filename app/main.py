"""Application entry point.

This module contains :
    - the app definition ;
    - the definition of base and public APIs ;
    - the import and concatenation other endpoints from routers.
"""
from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from app.config.config import Settings, get_base_settings
from app.core.api.routers import games, users
from app.core.api.security.authentication import (Token, authenticate_user,
                                                  create_access_token)
from app.dependencies import get_db_session

app_settings = get_base_settings()
app = FastAPI(title=app_settings.app_name,
              version=app_settings.app_version,
              docs_url=app_settings.url_swagger)
origins = [
    "http://localhost:5000",
    "http://w31q1.ddns.net:5000",
    "http://w31q1.ddns.net:80",
    "http://w31q1.ddns.net",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(users.router)
app.include_router(games.router)


@app.get("/", response_model=Settings)
def read_root(settings: Settings = Depends(get_base_settings)):
    """Return some basic application informations."""
    return settings


@app.get("/favicon", response_class=FileResponse)
def read_favicon():
    """Return the application favicon.

    Returns:
        FileResponse: favicon.ico
    """
    return FileResponse("app/filer/static/favicon.ico")


@app.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends(),
                                 database: Session = Depends(get_db_session)):
    """Create a token to be allowed to use secured APIs. Valid credentials must be provided."""
    user = authenticate_user(database, form_data.username, form_data.password)

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token = create_access_token(data={"sub": user.user_name})
    return {"access_token": access_token, "token_type": "bearer"}
