"""Shared API dependencies which should be executed BEFORE running the API call."""
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError
from sqlalchemy.orm import Session

from app.core.api.security.authentication import validate_jwt_token
from app.core.datasource.database.connector import Base, SessionLocal, engine
from app.core.entity.crud.user import get_user_by_username
from app.core.entity.pymodel.user import User

# Initialize a declarative base
Base.metadata.create_all(bind=engine)
# Initialize the token bearer logic
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def get_db_session() -> Session:
    """Get a database session to be used for the process to execute database interaction.

    This will be a mandatory dependency for any API related to the database (like CRUD operations).

    Yields:
        Session: An active session.
    """
    database = SessionLocal()
    try:
        yield database
    finally:
        database.close()


def get_current_user(token: str = Depends(oauth2_scheme), database: Session = Depends(get_db_session)) -> User:
    """Get the current user informations based on the token.

    Raises:
        HTTPException: 401 if either the token or the user provided in the token are invalid.

    Returns:
        User: User details.
    """
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        token_data = validate_jwt_token(token)
    except JWTError:
        # Disabled pylint : we do not want to re-raise the JWTError for security purposes.
        raise credentials_exception  # pylint: disable=raise-missing-from

    user = get_user_by_username(database, token_data.username)
    if user is None:
        raise credentials_exception

    return user
