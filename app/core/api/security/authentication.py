"""This module focus on authentication management in the application."""
from datetime import datetime, timedelta
from typing import Optional

from app.core.entity.crud.user import get_user_by_username
from app.core.entity.pymodel.user import User
from jose import JWTError, jwt
from passlib.context import CryptContext
from pydantic import BaseModel
from sqlalchemy.orm import Session

# JWD Token encryption algorithm
ALGORITHM = "HS256"

# Password encryption object initialization.
# The used scheme should not be changed as it is being used to generate actual passwords.
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


class Token(BaseModel):
    """Representation of a token to be returned by the API.

    Attributes:
        access_token (str): A hashed token.
        token_type (int): Type of token (e.g. 'bearer')
    """

    access_token: str
    token_type: str


class TokenData(BaseModel):
    """Representation of the object to hash in the token. Currently : only the username."""

    username: Optional[str] = None


def verify_password(plain_password: str, hashed_password: str) -> bool:
    """Verify if the password matches with the hashed password.

    Beware of the has

    Args:
        plain_password (str): A not hashed password (usually coming from a API call).
        hashed_password (str): A hashed password (usually coming from the database).

    Returns:
        bool: True if the passwords are a match.
    """
    return pwd_context.verify(plain_password, hashed_password)


def authenticate_user(database: Session, username: str, password: str) -> Optional[User]:
    """Authenticate the user against the database.

    Both username and password must match. In case of error the function won't tell which one was wrong (standard \
    security pattern).

    Args:
        database (Session): Active database session.
        username (str): User's username.
        password (str): User's plain text password.

    Returns:
        User: Returns the user details if authenticated. `None` if authentication gone wrong.
    """
    user = get_user_by_username(database, username)
    if not user:
        return None
    if not verify_password(password, user.user_pwd):
        return None
    return user


def create_access_token(data: dict, expires_delay_in_sec: Optional[int] = 5400) -> str:
    """Create a valid access token.

    The encoding will be based on `JWT_SECRET_KEY` environment variable and algorithm specified in `ALGORITHM` python \
    variable.

    Args:
        data (dict): Data to encode in the token.
        expires_delay_in_sec (Optional[int], optional): Duraton of the token validity in seconds. Defaults to 5400 \
        (1 hour and half).

    Returns:
        str: The encoded token.
    """
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(seconds=expires_delay_in_sec)
    to_encode.update({"exp": expire})

    encoded_jwt = jwt.encode(to_encode,
                             "c98c9dad56ed0d5119939aab3871285464b4e8f4d369d0a08d1588fb219c87b2",
                             algorithm=ALGORITHM)

    return encoded_jwt


def validate_jwt_token(jwt_token: str) -> TokenData:
    """Validate a JWT token vs user.

    Args:
        jwt_token (str): Encoded JWT token to validate.

    Raises:
        JWTError: If the decoding of the token failed for any reason.

    Returns:
        TokenData: The decoded JWT relevant data.
    """
    payload = jwt.decode(jwt_token,
                         "c98c9dad56ed0d5119939aab3871285464b4e8f4d369d0a08d1588fb219c87b2",
                         algorithms=[ALGORITHM])
    username: str = payload.get("sub")
    if username is None:
        raise JWTError("Username missing from payload")
    return TokenData(username=username)
