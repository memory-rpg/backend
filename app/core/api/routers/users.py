"""Router for all "users/" endpoints."""
from app.core.entity.pymodel.user import User
from app.dependencies import get_current_user
from fastapi import APIRouter, Depends

router = APIRouter(prefix="/users",
                   tags=["users"],
                   dependencies=[Depends(get_current_user)])


@router.get("/me", response_model=User)
async def read_user_me(current_user: User = Depends(get_current_user)):
    """Get the current user informations."""
    return current_user
