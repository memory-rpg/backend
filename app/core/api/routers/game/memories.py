"""Router for all "memories/" endpoints."""
from typing import List, Optional, Union

from app.core.domain.transformer.text_blurrer import get_memory_file_content
from app.core.entity.crud.memory import get_memories, get_memory_by_id
from app.core.entity.dbmodel.user import User
from app.core.entity.pymodel.memory import Memory
from app.dependencies import get_current_user, get_db_session
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

router = APIRouter(prefix="/memories",
                   tags=["memories"])


@router.get("/", response_model=List[Memory])
async def read_memories(game_id: int, database: Session = Depends(get_db_session), user: User = Depends(get_current_user)):
    """Return all memories from database."""
    try:
        return get_memories(database, user.id, game_id)
    except PermissionError as permission_err:
        raise HTTPException(status_code=401,
                            detail="Memories cannot be accessed by the user.") from permission_err


@router.get("/{memory_id}", response_model=Union[Memory, str])
async def read_memory(game_id: int,
                      memory_id: int,
                      blurred_content: Optional[bool] = False,
                      database: Session = Depends(get_db_session),
                      user: User = Depends(get_current_user)):
    """Return a specific memory based on the ID with its blurred text version."""
    try:
        memory = get_memory_by_id(database, user.id, game_id, memory_id)
    except PermissionError as permission_err:
        raise HTTPException(status_code=401,
                            detail=f"Memory id {memory_id} cannot be accessed by the user.") from permission_err

    if memory is None:
        raise HTTPException(status_code=404, detail=f"Memory not found with id {memory_id}")

    if blurred_content:
        if memory.blurred_file_path:
            return get_memory_file_content(memory.blurred_file_path)
        raise HTTPException(status_code=404, detail=f"Not blurred memory associated to memory id {memory_id}")

    return memory
