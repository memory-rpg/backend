"""Router for all "items/" endpoints."""
from typing import List

from app.core.entity.crud.item import get_item_by_id, get_items
from app.core.entity.dbmodel.user import User
from app.core.entity.pymodel.item import Item, ItemLight
from app.dependencies import get_current_user, get_db_session
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

router = APIRouter(prefix="/items",
                   tags=["items"])


@router.get("/", response_model=List[ItemLight])
async def read_items(game_id: int, database: Session = Depends(get_db_session), user: User = Depends(get_current_user)):
    """Return all items from database.

    Additional notes :
        - It will calculate the "Ligth" version of the items without other attached elements details.
        - To see details of the item, use `/items/{item_id}`
    """
    try:
        return get_items(database, game_id, user.id)
    except PermissionError as permission_err:
        raise HTTPException(status_code=401,
                            detail="Items cannot be accessed by the user.") from permission_err


@router.get("/{item_id}", response_model=Item)
async def read_item(game_id: int, item_id: int, database: Session = Depends(get_db_session), user: User = Depends(get_current_user)):
    """Return a specific item based on the provided id.

    Additional notes : The item will contains the list of `Memory` associated to it.
    """
    try:
        item = get_item_by_id(database, game_id, user.id, item_id)
    except PermissionError as permission_err:
        raise HTTPException(status_code=401,
                            detail="Items cannot be accessed by the user.") from permission_err
    if item is None:
        raise HTTPException(status_code=404, detail=f"Item not found with id {item_id}")
    return item
