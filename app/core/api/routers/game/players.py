"""Router for all "players/" endpoints."""
from typing import List, Optional

from app.core.entity.crud.player import (get_player_by_id,
                                         get_player_for_user_game, get_players)
from app.core.entity.dbmodel.user import User
from app.core.entity.pymodel.player import Player
from app.dependencies import get_current_user, get_db_session
from fastapi import APIRouter, Depends, HTTPException, Query
from sqlalchemy.orm import Session

router = APIRouter(prefix="/players",
                   tags=["players"])


@router.get("/", response_model=List[Player])
async def read_players(game_id: int,
                       player_types: Optional[List[str]] = Query(["PC"]),
                       exclude_me: Optional[bool] = Query(False),
                       database: Session = Depends(get_db_session),
                       user: User = Depends(get_current_user)):
    """Return all players from database."""
    return get_players(database, game_id, player_types, exclude_me, user.id)


@router.get("/me", response_model=Player)
async def read_player_me(game_id: int, database: Session = Depends(get_db_session), user: User = Depends(get_current_user)):
    """Return current user's player details."""
    player = get_player_for_user_game(database, game_id, user.id)
    if player is None:
        raise HTTPException(status_code=404, detail=f"No player is associated with the user '{user.user_name}'.")
    return player


@router.get("/{player_id}", response_model=Player)
async def read_player(game_id: int, player_id: int, database: Session = Depends(get_db_session)):
    """Return a specific player specified by its id from database."""
    player = get_player_by_id(database, game_id, player_id)
    if player is None:
        raise HTTPException(status_code=404, detail=f"Player not found with id {player_id}")
    return player
