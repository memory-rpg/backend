"""Router for all "items/" endpoints."""
import logging
from typing import List, Optional

from app.core.api.routers.game import items, memories, players
from app.core.domain.behavior.memory_manager import \
    get_blurred_memory_with_item_code
from app.core.entity.crud.game import get_game, get_games
from app.core.entity.dbmodel.user import User
from app.core.entity.pymodel.game import GameBase
from app.dependencies import get_current_user, get_db_session
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

# sub-router for /games API endpoints with a game id : will be the root of all other API endpoints
router_game = APIRouter(prefix="/{game_id}",
                        tags=["games"],
                        dependencies=[Depends(get_current_user)])


@router_game.get("/", response_model=GameBase)
async def read_game(game_id: int, database: Session = Depends(get_db_session)):
    game = get_game(database, game_id)
    if not game:
        raise HTTPException(status_code=404, detail=f"Game not found with id {game_id}")
    return game


@router_game.post("/discover-memory", tags=["memories", "items"], response_model=int)
async def discover_memory_via_item(game_id: int,
                                   item_code: str,
                                   mood_color: Optional[str] = None,
                                   database: Session = Depends(get_db_session),
                                   user: User = Depends(get_current_user)):
    try:
        memory_int = get_blurred_memory_with_item_code(database, game_id, user.id, item_code, mood_color)
    except PermissionError as permission_err:
        logging.info("User %s tried to access items in game id %s. but was not authorized.",
                     user.id,
                     game_id)
        raise HTTPException(status_code=401,
                            detail="Items cannot be accessed by the user.") from permission_err
    except ValueError as value_err:
        raise HTTPException(status_code=404,
                            detail=f"""Either item with code {item_code} does not exist or not memory
                                                    has been associated to it for user {user.id}""") from value_err

    return memory_int


router_game.include_router(memories.router)
router_game.include_router(players.router)
router_game.include_router(items.router)

# Root router for /games API endpoints
router = APIRouter(prefix="/games",
                   tags=["games"])


@router.get("/", response_model=List[GameBase])
async def read_games(database: Session = Depends(get_db_session)):
    return get_games(database)


router.include_router(router_game)
