"""Pydantic modelisation for Game object/table."""
from typing import Optional

from pydantic import BaseModel


class GameBase(BaseModel):
    """Mandatory fields for a Game."""

    id: int
    name: str
    start_date_timestamp: Optional[int] = None
    end_date_timestamp: Optional[int] = None
    is_player_status_impacting: Optional[int] = 1
    is_mood_color_impacting: Optional[int] = 1

    class Config:
        orm_mode = True
