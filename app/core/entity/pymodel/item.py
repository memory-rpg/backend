"""Pydantice modelisation for Item object/table."""
from typing import List

from app.core.entity.pymodel.memory import Memory
from pydantic import BaseModel


class ItemBase(BaseModel):
    """Mandatory fields for an Item."""

    id: int
    code: str
    name: str
    game_id: int


class ItemLight(ItemBase):
    """Describe an item without its relashion with other table elements. Avoid transmitting too many information."""

    description: str

    class Config:
        orm_mode = True


class Item(ItemLight):
    """Complete item object including attached information."""

    memories: List[Memory] = []

    class Config:
        orm_mode = True
