"""Pydantic modelisation for User object/table."""
from pydantic import BaseModel


class User(BaseModel):
    """User informations to be used in application."""

    id: int
    user_name: str

    class Config:
        orm_mode = True


class UserSecure(User):
    """This object should not be used in the API code part."""

    user_pwd: str
