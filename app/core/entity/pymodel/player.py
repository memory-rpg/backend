"""Pydantic modelisation for Player object/table : highly related to User object/table."""
from enum import Enum
from typing import List, Optional

from app.core.entity.pymodel.memory import Memory
from pydantic import BaseModel


class PlayerStatus(Enum):
    GOOD = "GOOD"
    INJURED = "INJURED"
    BAD = "BAD"

    @property
    def blur_coeff(self) -> float:
        match self:
            case PlayerStatus.GOOD: return 0.0
            case PlayerStatus.INJURED: return 0.3
            case _: return 0.8


class PlayerType(Enum):
    """Enumerate the possible type of players.

    This is hard-linked to the database content `Player.player_type`.
    """

    # Game Master (or Guru Mister or Great Master depending on your own vision) : there is only one to rule them all !
    GM = "GM"
    # Played Character : typical player.
    PC = "PC"
    # Non-Played Character : for lore or backup players.
    NPC = "NPC"


class PlayerBase(BaseModel):
    id: int
    game_id: int
    user_id: int


class PlayerLight(PlayerBase):
    character_name: str
    player_type: str
    player_status: Optional[str] = None

    class Config:
        orm_mode = True


class Player(PlayerLight):

    memories: List[Memory] = []

    class Config:
        orm_mode = True
