"""Pydantic modelisation for Memory object/table."""
from typing import Optional

from pydantic import BaseModel


class MemoryBase(BaseModel):
    """Mandatory fields for a Memory."""

    id: int
    base_file_path: str


class Memory(MemoryBase):
    """Complete Memory object."""

    item_id: int
    player_id: int
    blurred_file_path: Optional[str] = None
    blurred_timestamp: Optional[int] = None
    intensity: Optional[int] = 100
    mood_color: Optional[str] = None

    class Config:
        orm_mode = True
