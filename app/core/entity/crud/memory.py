"""CRUD methods for memories."""
import logging
from typing import List

from app.core.entity.crud.player import get_player_for_user_game
from app.core.entity.dbmodel.memory import Memory as MemoryDb
from app.core.entity.pymodel.memory import Memory as MemoryPy
from app.core.entity.pymodel.player import PlayerType
from sqlalchemy.orm.query import Query
from sqlalchemy.orm.session import Session

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def get_specific_filters_for_memory(database: Session, game_id: int, user_id: int) -> Query:
    player = get_player_for_user_game(database,  game_id, user_id)
    if not player:
        raise PermissionError(f"User '{user_id}' cannot access memories list (not a player).")

    query = database.query(MemoryDb)

    match PlayerType(player.player_type):
        case PlayerType.GM: pass
        case PlayerType.PC:
            query = query.filter(MemoryDb.player_id == player.id)
            query = query.filter(MemoryDb.blurred_file_path.isnot(None))
        case _:
            raise PermissionError(f"Player '{player.id}' cannot access memories list ('{player.player_type}' type).")

    return query


def get_memories(database: Session, user_id: int, game_id: int) -> List[MemoryPy]:
    query = get_specific_filters_for_memory(database, game_id, user_id)
    return query.all()


def get_memory_by_id(database: Session, user_id: int, game_id: int, memory_id: int) -> MemoryPy:
    query = get_specific_filters_for_memory(database, game_id, user_id)
    return query.filter(MemoryDb.id == memory_id).first()


def update_memory(database: Session, memory: MemoryPy) -> bool:
    nb_rows_updated = database.query(MemoryDb).filter(
        MemoryDb.id == memory.id).update({
            MemoryDb.blurred_timestamp: memory.blurred_timestamp,
            MemoryDb.blurred_file_path: memory.blurred_file_path
        })
    if nb_rows_updated == 1:
        database.commit()
        return True

    logger.error("There was an issued during the memory %s update, %s rows have been updated (expected : 1).",
                 memory.id,
                 nb_rows_updated)
    return False
