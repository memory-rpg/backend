from typing import List, Optional

from app.core.entity.dbmodel.player import Player as PlayerDb
from app.core.entity.pymodel.player import Player as PlayerPy
from app.core.entity.pymodel.player import PlayerLight
from sqlalchemy.orm import Session


def get_players(database: Session, game_id: int,
                player_types: List[str],
                exclude_me: bool,
                user_id: Optional[int] = None) -> List[PlayerLight]:
    query = database.query(PlayerDb)
    query = query.filter(PlayerDb.player_type.in_(player_types))
    if exclude_me and user_id:
        query = query.filter(PlayerDb.user_id != user_id)
    return query.filter(PlayerDb.game_id == game_id).all()


def get_player_by_id(database: Session, game_id: int, player_id: int) -> PlayerPy:
    return database.query(PlayerDb).filter(PlayerDb.id == player_id).filter(PlayerDb.game_id == game_id).first()


def get_player_for_user_game(database: Session, game_id: int, user_id: int) -> PlayerPy:
    return database.query(PlayerDb).filter(PlayerDb.user_id == user_id).filter(PlayerDb.game_id == game_id).first()
