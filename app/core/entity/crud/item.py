"""CRUD methods for items."""
from typing import List

from app.core.entity.crud.player import get_player_for_user_game
from app.core.entity.dbmodel.item import Item as ItemDb
from app.core.entity.pymodel.item import Item, ItemLight
from sqlalchemy.orm.query import Query
from sqlalchemy.orm.session import Session


def get_specific_filters_for_item(database: Session, game_id: int, user_id: int) -> Query:
    player = get_player_for_user_game(database,  game_id, user_id)
    if not player:
        raise PermissionError("User '{user_id}' cannot access items list (not a player).")

    query = database.query(ItemDb)
    query = query.filter(ItemDb.game_id == game_id)

    return query


def get_items(database: Session, game_id: int, user_id: int) -> List[ItemLight]:
    query = get_specific_filters_for_item(database, game_id, user_id)
    return query.all()


def get_item_by_id(database: Session, game_id: int, user_id: int, item_id: int) -> Item:
    query = get_specific_filters_for_item(database, game_id, user_id)
    return query.filter(ItemDb.id == item_id).first()


def get_item_by_code(database: Session, game_id: int, item_code: str) -> Item:
    return database.query(ItemDb).filter(ItemDb.code == item_code).filter(ItemDb.game_id == game_id).first()
