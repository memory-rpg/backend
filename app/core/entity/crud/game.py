"""CRUD methods for items."""
from typing import List

from app.core.entity.dbmodel.game import Game as GameDb
from app.core.entity.pymodel.game import GameBase as GamePy
from sqlalchemy.orm.session import Session


def get_games(database: Session) -> List[GamePy]:
    return database.query(GameDb).all()


def get_game(database: Session, game_id: int) -> GamePy:
    return database.query(GameDb).filter(GameDb.id == game_id).first()
