from typing import Optional

from app.core.entity.dbmodel.user import User as UserDb
from app.core.entity.pymodel.user import User as UserPy
from sqlalchemy.orm import Session


def get_user_by_username(database: Session, username: str) -> Optional[UserPy]:
    return database.query(UserDb).filter(UserDb.user_name == username).first()
