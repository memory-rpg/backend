"""Database modelisation for Game table."""
from app.core.datasource.database.connector import Base
from app.core.entity.dbmodel import SCHEMA_GAME
from sqlalchemy import Column, Integer, String


class Game(Base):
    __tablename__ = "Game"
    __table_args__ = {'schema': SCHEMA_GAME}

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    start_date_timestamp = Column(Integer)
    end_date_timestamp = Column(Integer)
    is_player_status_impacting = Column(Integer)
    is_mood_color_impacting = Column(Integer)
