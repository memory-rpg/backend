"""Database modelisation for Item table."""
from app.core.datasource.database.connector import Base
from app.core.entity.dbmodel import SCHEMA_GAME
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship


class Item(Base):
    __tablename__ = "Item"
    __table_args__ = {'schema': SCHEMA_GAME}

    id = Column(Integer, primary_key=True, index=True)
    code = Column(String, unique=True)
    name = Column(String, unique=True)
    description = Column(String)
    game_id = Column(Integer)

    memories = relationship("Memory", back_populates="item")
