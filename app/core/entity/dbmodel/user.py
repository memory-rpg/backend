"""Database modelisation for User table."""
from app.core.datasource.database.connector import Base
from app.core.entity.dbmodel import SCHEMA_CORE
from sqlalchemy import Column, Integer, String


class User(Base):
    """Representation of an user.

    An `User` have an implicit relationship with `Player` which is not modelized because they are in separate schemas. \
    So it will be programmatically managed.
    """

    __tablename__ = "User"
    __table_args__ = {'schema': SCHEMA_CORE}

    id = Column(Integer, primary_key=True, index=True)
    user_name = Column(String)
    user_pwd = Column(String)
