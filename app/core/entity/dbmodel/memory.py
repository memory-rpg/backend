"""Database modelisation for Memory table."""
from app.core.datasource.database.connector import Base
from app.core.entity.dbmodel import SCHEMA_GAME
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship


class Memory(Base):
    __tablename__ = "Memory"
    __table_args__ = {'schema': SCHEMA_GAME}

    id = Column(Integer, primary_key=True, index=True)
    base_file_path = Column(String, unique=True)
    blurred_file_path = Column(String, unique=True)
    blurred_timestamp = Column(Integer)
    intensity = Column(Integer)
    mood_color = Column(String)
    item_id = Column(Integer, ForeignKey(SCHEMA_GAME + ".Item.id"))
    player_id = Column(Integer, ForeignKey(SCHEMA_GAME + ".Player.id"))

    item = relationship("Item", back_populates="memories")
    player = relationship("Player", back_populates="memories")
