"""Database modelisation for Player table."""
from app.core.datasource.database.connector import Base
from app.core.entity.dbmodel import SCHEMA_GAME
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship


class Player(Base):
    """Representation of an player.

    An `User` have an implicit relationship with `Player` which is not modelized because they are in separate schemas. \
    So it will be programmatically managed.
    """

    __tablename__ = "Player"
    __table_args__ = {'schema': SCHEMA_GAME}

    id = Column(Integer, primary_key=True, index=True)
    character_name = Column(String, unique=True)
    player_type = Column(String)
    player_status = Column(String)
    user_id = Column(Integer)
    game_id = Column(Integer)

    memories = relationship("Memory", back_populates="player")
