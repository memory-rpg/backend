"""Managed methods related to memories manipulation and extraction."""
import datetime
import logging
from os import path
from typing import Optional

from app.core.domain.transformer.text_blurrer import (blur_html_file)
from app.core.entity.crud.item import get_item_by_code
from app.core.entity.crud.memory import update_memory
from app.core.entity.crud.player import get_player_for_user_game
from app.core.entity.pymodel.memory import Memory
from app.core.entity.pymodel.player import PlayerStatus
from app.core.utils import calc_colors_proximity
from sqlalchemy.orm import Session

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
# Proportion of the color distance which is applied to the clarity value
MEM_FEATURE_MOOD_PROPORTION = 3
# Check documentation\domain\memory_mechanic.md for details
MEM_FEATURE_BASE_CLARITY_VALUE = 50


def get_blurred_memory_with_item_code(database: Session,
                                      game_id: int,
                                      user_id: int,
                                      item_code: str,
                                      mood_color: Optional[str] = None) -> int:
    """Blur a memory associated to an item for a given player.

    Blur will take place only if, for the provided item, there is a memory associated to the player.
    Blur effect will be affected by `mood_color` and the player status (see `PlayerStatus`).
    If the memory has already been blurred once, then it will not be done again, instead the previously blurred memory \
    will be returned again.

    Obviously, `game_id` and  `item_code` must be valids.

    The function will :
        - generate a new file (containing the frozen memory content) ;
        - update the associated memory entry (with the path to new file).

    Args:
        database (Session): Active database session.
        game_id (int): ID of the game related to the item.
        user_id (int): ID of the user querying the item.
        item_code (str): CODE of the item (different from its ID !).
        mood_color (Optional[str]): A color described in hexadecimal (e.g. "ffffff"). Defaults to `None`.

    Raises:
        PermissionError: If the user is not a player in that game.
        ValueError: If no item or memory matches the criteria.

    Returns:
        int: The id of the blurred memory.
    """
    player = get_player_for_user_game(database, game_id, user_id)
    if not player:
        err_msg = f"User '{user_id}' cannot access items list (not a player)."
        logger.warning(err_msg)
        raise PermissionError(err_msg)

    item = get_item_by_code(database, game_id, item_code)
    if not item:
        err_msg = f"There is no item with code '{item_code}'."
        logger.warning(err_msg)
        raise ValueError(err_msg)

    try:
        memory = next(memory for memory in item.memories if memory.player_id == player.id)

        # Memory is being frozen once accessed : we then return always the same with a warning log.
        if memory.blurred_file_path:
            logger.warning("Since %s, memory %s already have a blurred content : %s.",
                           memory.blurred_timestamp,
                           memory.id,
                           memory.blurred_file_path)
            return memory.id

        # Calculate clarity player value
        clarity_value = calculate_clarity_value(memory, PlayerStatus(player.player_status), mood_color)

        # Randomly blur the memory (clarity value limit the randomness)
        blurred_path = blur_html_file(memory.base_file_path, clarity_value)

        # Update the database entry
        memory.blurred_file_path = path.basename(blurred_path)
        memory.blurred_timestamp = datetime.datetime.now().timestamp()
        update_memory(database, memory)

        # Eventually, returns the blurred content to the caller
        return memory.id

    except StopIteration as stop_it:
        err_msg = f"No memory for item {item.id} associated with player {player.id}."
        logger.warning(err_msg)
        raise ValueError(err_msg) from stop_it


def calculate_clarity_value(memory: Memory,
                            player_status: Optional[PlayerStatus] = None,
                            player_mood_color: Optional[str] = None) -> int:
    clarity_value = MEM_FEATURE_BASE_CLARITY_VALUE
    if player_status:
        clarity_value *= (1 - player_status.blur_coeff)
        logger.info("Clarity value is %s after being reduced by %s (player status).",
                    clarity_value,
                    player_status.blur_coeff)
    if player_mood_color:
        mood_proximity = calc_colors_proximity(player_mood_color, memory.mood_color) / 100
        if mood_proximity < 1:
            clarity_value *= (1 - mood_proximity/MEM_FEATURE_MOOD_PROPORTION)
        logger.info("Clarity value is %s after being reduced by %s (mood color difference).",
                    clarity_value,
                    mood_proximity)

    clarity_value *= (1 + memory.intensity / 100)
    logger.info("Clarity value is %s after being improved by %s (memory intensity).",
                clarity_value,
                memory.intensity)

    logger.info("Final memory value is : %s", clarity_value)

    return clarity_value
