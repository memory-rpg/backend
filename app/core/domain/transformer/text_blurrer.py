"""Methods related to text blurring."""
import logging
import re
from os import path
from random import randint

from app.config.config import get_filer_settings
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)


def blur_words(words: str, blur_probability: int) -> str:
    """Apply some "blur" on a string with some random.

    Randomness will be calculated by comparing a `randint(0, 100)` and `blur_probability`. All words in the string will \
    either blurred or not (they are not tested separatly).

    Spaces are kept visibles to let words shape visible.

    Args:
        words (str): Some words to blur.
        blur_probability (int): Probability for the words : 100 means nothing will be visible, 0 means everything will \
    be visible.

    Returns:
        str: A possibly blurred string.
    """
    clarity_probability = randint(0, 100)  # nosec : related to gameplay, not security
    if blur_probability > 0 and clarity_probability <= blur_probability:
        # Do not blur spaces to let words shape visible
        return re.sub("[^ \\W]", "█", words)

    return words


def blur_text_html(html_file: str, blur_probability: float) -> str:
    """Apply blur effect word on an html file with some randomness.

    Some tags have impact on the blur logic :
        - del : will always be blurred.
        - mark : will never be blurred.
        - p : words contained in it will be candidate for blur.

    Text outside <p> won't be considered.

    Args:
        html_file (str): A valid HTML file to blur.
        blur_probability (float): The base blur probability (between 0 and 100) to apply on words (100 = blur, 0 = no \
    blur).

    Returns:
        str: A plain text without any html tag.
    """
    soup = BeautifulSoup(html_file, "html.parser")

    if soup.body:
        for text_to_blur in soup.find_all("p"):
            changed_text = ""
            for text in text_to_blur.find_all(text=True):
                match text.parent.name:
                    case "del":
                        changed_text += blur_words(text, 100)
                    case "mark":
                        changed_text += blur_words(text, 0)
                    case _:
                        changed_text += " ".join([blur_words(word, blur_probability) for word in text.split()]) + " "

            text_to_blur.replace_with(changed_text)

        return soup.body.text

    logger.warning("""We were not able to find a <body> tag in the HTML file, so the file will be converted like a \
                    plain text file. We recommend them to be stored as valid HTML files for complete compatibility.""")
    return " ".join([blur_words(word, blur_probability) for word in soup.text.split()])


def blur_html_file(html_file_path: str, clarity_value: float) -> str:
    """Open a file, apply a blur on it and return its blurred version as string.

    Args:
        html_file_path (str): Path of the file containing the html text.
        clarity_value (float): Ability of the player to remember a given text. Between [0;100].

    Returns:
        str: Blurred HTML file path.
    """
    blur_probability = 100 - clarity_value
    with open(path.join(get_filer_settings().memories_content_path, html_file_path), encoding="UTF-8") as html_file:
        blurred_content = blur_text_html(html_file, blur_probability)
        blurred_file_path = path.join(get_filer_settings().memories_content_path, ("blurred_" + html_file_path))
        with open(blurred_file_path, mode="w+", encoding="UTF-8") as blurred_html_file:
            blurred_html_file.write(blurred_content)

        return blurred_file_path


def get_memory_file_content(memory_filename: str) -> str:
    """Extract memory file content.

    Args:
        memory_filename (str): Name of the memory in the filer (should be identified in `Memory.blurred_file_path`).

    Returns:
        str: Blurred HTML file as plain text.
    """
    with open(path.join(get_filer_settings().memories_content_path, memory_filename), encoding="UTF-8") as file:
        return file.read()
