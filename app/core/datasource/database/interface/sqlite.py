"""Database helper implementation for SQLite3 databases."""
import logging
import os.path

from app.config.config import get_database_settings
from app.core.datasource.database.interface.common import (DatabaseHelper,
                                                           ManagedDatabase)
from sqlalchemy import create_engine
from sqlalchemy.engine.base import Engine
from sqlalchemy.event.api import listen
from sqlalchemy.exc import OperationalError

settings = get_database_settings()


def listener_connect(engine, _):  # pragma: no cover
    """Listen to "connect" events.

    When a "connect" event occurs, this method do attach database again. This is due to the SQLite needs to re-attach \
    `in-memory` database before each connection.

    Second parameter of that overriden method is unused so we ignore it.

    Args:
        engine (Engine): Should be the initialized engine to listen.
        rec (Any): Unknown.
    """
    logging.debug("Database 'connect' event detected.")
    if engine is not None:
        logging.debug("Re-attach databases")
        DatabaseHelper.execute_script(f"{settings.database_scripts_path}attach/databases_attach.sql",
                                      engine,
                                      False)


class SQLiteHelper(DatabaseHelper):
    """SQLite `DatabaseHelper` implementation."""

    def __init__(self) -> None:
        """Initialize SQLiteHelper.

        At init time the existence of the database is tested : if it does not exist, it will be attempted to \
        initialize the database model and data in `init_database_connection()`.
        """
        super().__init__(ManagedDatabase.SQLITE)
        self._db_url = settings.database_url
        self._connect_args = settings.database_connect_args
        self._exists_before_startup = os.path.isfile(self._db_url)
        if not self._exists_before_startup:
            logging.info("Database file %s does not exist yet.", self._db_url)

    def init_database_connection(self, init_if_empty: bool = True, force_init: bool = False) -> Engine:
        """Initialize a SQLAlchemy engine with an active database connection.

        Args:
            init_if_empty (bool, optional): If set to `True` and if the database did not exist, it will try to execute \
        init scripts against it. Defaults to True.
            force_init (bool, optional): If set to `True`, it will try to execute init scripts against it. Defaults to \
        False.

        Returns:
            Engine: A fully operational SQLAlchemy engine connected to the SQLite database.
        """
        engine = create_engine(("sqlite:///" + self._db_url),
                               connect_args=self._connect_args)

        listen(engine, "connect", listener_connect)

        try:
            if (init_if_empty and not self._exists_before_startup) or force_init:
                logging.info("Database will be initialized.")
                # Attach databases to allows SQLite to work with a "schema" logic
                DatabaseHelper.execute_scripts(f"{settings.database_scripts_path}init/core/", engine)
                DatabaseHelper.execute_scripts(f"{settings.database_scripts_path}init/game/", engine)
                self._exists_before_startup = False
        except OperationalError as op_err:
            # Clean db before re-raising the exception
            logging.warning("Exception occured while initializing the database.")
            if os.path.isfile(self._db_url):  # pragma: no cover
                logging.warning("Removing `%s`.", self._db_url)
                os.remove(self._db_url)
            raise op_err

        return engine
