"""Describe common classes for Database management."""

from __future__ import annotations

from enum import Enum
from os import path, walk

from sqlalchemy import text
from sqlalchemy.engine.base import Engine


class ManagedDatabase(Enum):
    """Describe currently supported database."""

    SQLITE = "sqlite"
    # PGSQL = "postgresql"  # No supported
    # MYSQL = "mysql"  # No supported
    # SQLSERVER = "sqlserver"  # No supported

    @staticmethod
    def get_enum_from_value(value: str) -> ManagedDatabase:
        """Get the correct enum based on the value.

        Args:
            value (str): Enum string value to match.

        Raises:
            ValueError: Raised if `value` does not match an enum

        Returns:
            ManagedDatabase:ManagedDatabase enum.
        """
        match value:
            case ManagedDatabase.SQLITE.value: return ManagedDatabase.SQLITE
            case _: raise ValueError(f"{value} if not a supported database type.")


class DatabaseHelper():
    """Describe a generic database helper.

    It must be inherinted by real database specialized implementation (SQLite, MySQL, ...).
    """

    def __init__(self, database_type: ManagedDatabase):
        """Initialize `DatabaseHelper` (should be done by child class).

        Args:
            database_type (ManagedDatabase): Type of implemented database helper.
        """
        self._database_type = database_type

    def init_database_connection(self,
                                 init_if_empty: bool = True,
                                 force_init: bool = False) -> Engine:  # pragma: no cover
        raise NotImplementedError("To be implemented in children classes.")

    @classmethod
    def execute_script(cls, sql_script_file: str, engine: Engine, use_text_clause=True) -> None:
        """Execute a SQL file script against an SQL engine.

        The file will be parsed and instructions will be extracted + executed separatly.

        Important notices :
            - File must be in UTF-8 ;
            - SQL instructions can be on several lines but must ends with a ";" ;
            - Only "--" type of comment are supported.

        Args:
            sql_script_file (str): Path to the SQL script file.
            engine (Engine): SQL engine to execute the script.
            use_text_clause (bool): If `True` statement will be parsed as `TextClause`. Default to `True`.
        """
        with open(sql_script_file, encoding="UTF-8") as sql_file:
            sql_command = ""
            for line in sql_file:
                # Ignore SQL comments
                if not line.strip().startswith('--') and line.strip('\n'):
                    sql_command += line.strip()
                    if sql_command.endswith(";"):
                        # Execute the built command
                        if use_text_clause:
                            sql_command = text(sql_command)
                        engine.execute(sql_command)
                        sql_command = ""

    @classmethod
    def execute_scripts(cls, sql_script_folder: str, engine) -> None:
        """Execute all scripts in a folder in alphanumerical order.

        The method will scan all files with ".sql" extension, order then with `sorted()` and then execute them \
        sequentially and individually.

        Important notices :
            - All files in folder must be SQL executable ;
            - Sub-folders will be ignored ;
            - Check `execute_script()` for detailed scripts informations.

        Args:
            sql_script_folder (str): Path of the folder containing all scripts to execute.
            engine (Engine): SQL engine to execute the script.
        """
        _, _, filenames = next(walk(sql_script_folder), (None, None, []))
        filenames = [filename for filename in filenames if filename.lower().endswith(".sql")]
        for filename in sorted(filenames):
            cls.execute_script(path.join(sql_script_folder, filename), engine)
