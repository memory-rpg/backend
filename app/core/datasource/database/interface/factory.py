"""
This file contains the DatabaseHelper classes factory.

Update it to make more databases availables.
"""
from app.core.datasource.database.interface.common import (DatabaseHelper,
                                                           ManagedDatabase)
from app.core.datasource.database.interface.sqlite import SQLiteHelper


def database_helper_factory(database: ManagedDatabase) -> DatabaseHelper:
    """Instanciate the `DatabaseHelper` object related to the provided `ManagedDatabase`.

    Args:
        database (ManagedDatabase): Enumeration of the database to instanciate.

    Raises:
        NotImplementedError: If the provided database key is not yet supported by the application.

    Returns:
       DatabaseHelper: An instance of an implementation of `DatabaseHelper`.
    """
    match database:
        case ManagedDatabase.SQLITE: return SQLiteHelper()
        case _: raise NotImplementedError(f"Support of {database} is not implemented yet.")
