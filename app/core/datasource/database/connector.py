"""Declarative database methods."""
from app.config.config import get_database_settings
from app.core.datasource.database.interface.common import ManagedDatabase
from app.core.datasource.database.interface.factory import \
    database_helper_factory
from sqlalchemy.engine.base import Engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


def init_engine() -> Engine:
    """Initialize an engine based on the database selected in environment settings.

    Returns:
        Engine: SQLAlchemy engine initialized to the database.
    """
    database_type = ManagedDatabase.get_enum_from_value(get_database_settings().database_type)
    db_helper = database_helper_factory(database_type)
    return db_helper.init_database_connection()


engine = init_engine()
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()
