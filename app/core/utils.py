"""SPLIT ME when you add several function around the same subject."""

from functools import wraps
from typing import Any


def prevent_div_zero(wrapped_function):
    """Ensure that no argument is equal to zero.

    It will basically convert all args = 0 to 0.001.

    If you want some PRECISION -> DO NOT USE IT.
    """
    @wraps(wrapped_function)
    def apply_filter(*args, **kwargs) -> Any:
        args_as_list = list(args)
        for i, arg in enumerate(args_as_list):
            if arg == 0.0:
                args_as_list[i] = 0.0001
        return wrapped_function(*tuple(args_as_list), **kwargs)
    return apply_filter


def calc_colors_proximity(color_hex_1: str, color_hex_2: str) -> float:
    """Calculate proximity for 2 hexadecimal numbers.

    It will compare the proximity of red, green and blue separatly and then return the average.

    Args:
        color_hex_1 (str): Must be comprised between `000000` and `ffffff`
        color_hex_2 (str): Must be comprised between `000000` and `ffffff`

    Returns:
        float: An average proximity score between 0 and 100.
    """
    red = calc_value_percentage_ordered(int(color_hex_1[0:2], 16), int(color_hex_2[0:2], 16))
    green = calc_value_percentage_ordered(int(color_hex_1[2:4], 16), int(color_hex_2[2:4], 16))
    blue = calc_value_percentage_ordered(int(color_hex_1[4:6], 16), int(color_hex_2[4:6], 16))
    return round(sum([red, green, blue])/3, 2)


def calc_value_percentage_ordered(value_1: float, value_2: float) -> float:
    """Call `calc_value_percentage` with ordered values.

    Args:
        value_1 (float): Any float.
        value_2 (float): Any float.

    Returns:
        float: Result of `calc_value_percentage`.
    """
    return calc_value_percentage(value_1, value_2) if value_1 <= value_2 else calc_value_percentage(value_2, value_1)


@prevent_div_zero
def calc_value_percentage(value: float, total: float) -> float:
    """Calculate percentage of value on total with `alue*100 / total`.

    Division per 0 is prevented.

    Args:
        value (float): Should be the smallest value for most cases.
        total (float): Should be the biggest value for most cases.

    Returns:
        float: Rounded percentage (2 decimals).
    """
    return round(value*100 / total, 2)
