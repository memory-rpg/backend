"""Helper methods regarding the authentications process (token generation for specific users, HTTP header builder, ...)"""
from app.core.api.security.authentication import create_access_token
from fastapi.testclient import TestClient


def create_token_for_user(username: str):
    """Directly build a token without using API entry point.

    This method is hardlinked to the sub we want to store. For now : only username.
    """
    return create_access_token(data={"sub": username})


def create_token_for_user_via_api(client: TestClient, username: str):
    """Call the API to generate a token."""
    match username:
        case "Admin": data = {"grant_type": "password",
                              "username": "Admin",
                              "password": "_ch4ng3_M3_"}
        case "Guest": data = {"grant_type": "password",
                              "username": "Guest",
                              "password": "gueguest2022"}
        case "AnyPlayer_1": data = {"grant_type": "password",
                                    "username": "AnyPlayer_1",
                                    "password": "gueguest2022"}
        case "AnyPlayer_2": data = {"grant_type": "password",
                                    "username": "AnyPlayer_2",
                                    "password": "gueguest2022"}
        case "AnyPlayer_3": data = {"grant_type": "password",
                                    "username": "AnyPlayer_3",
                                    "password": "gueguest2022"}
        case "SuperGM": data = {"grant_type": "password",
                                "username": "SuperGM",
                                "password": "gueguest2022"}
        case "NoPlayer": data = {"grant_type": "password",
                                 "username": "NoPlayer",
                                 "password": "gueguest2022"}
        case _: ValueError(f"""'{username}' is not a valid test user : either add an option to that method or check \
                            app/tests/helper/data/database/init/core/init_02_data.sql to see which are possible \
                            options.""")

    response = client.post("/token",
                           data=data)
    return response.json()["access_token"]


def get_header_with_token(token: str):
    """Build header part with the authorization token.

    This method is hardlinked to the way we manage token in the application. For now : bearer token.
    """
    return {"Authorization": f"Bearer {token}"}
