# Test data

This folder contains some static base data to be used in various tests. By updating it, be careful to not remove limit case for other tests.