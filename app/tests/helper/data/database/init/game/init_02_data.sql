-- Clean data
DELETE FROM Game.Memory;
DELETE FROM Game.Item;
DELETE FROM Game.Player;
DELETE FROM Game.Game;

-- Base test data
INSERT INTO Game.Game (id, name) VALUES (1, "Demo Game");

---------- Users
-- User AnyPlayer_1
INSERT INTO Game.Player (id, character_name, player_type, player_status, user_id, game_id) VALUES (1, "Jean le bûcheron", "PC", "GOOD", 3, 1);
-- User AnyPlayer_2
INSERT INTO Game.Player (id, character_name, player_type, player_status, user_id, game_id) VALUES (2, "Emilie la maçonne", "PC", "GOOD", 4, 1);
-- User AnyPlayer_3 : they are expected to have NO item or memory linked
INSERT INTO Game.Player (id, character_name, player_type, player_status, user_id, game_id) VALUES (3, "Bronn la Plante Verte du groupe", "PC", "INJURED", 5, 1);
-- User SuperGM
INSERT INTO Game.Player (id, character_name, player_type, player_status, user_id, game_id) VALUES (4, "Cactus tout doux", "GM", NULL, 6, 1);
-- User NPC
INSERT INTO Game.Player (id, character_name, player_type, player_status, user_id, game_id) VALUES (5, "Juste pour voir", "NPC", NULL, 2, 1);

---------- Items
-- Related to AnyPlayer_1 (only)
INSERT INTO Game.Item (id, code, name, description, game_id) VALUES (1, "xVZ63a", "Badge", "Le badge d'une femme morte allongée au sol.", 1);
-- Related to AnyPlayer_1 and AnyPlayer_2
INSERT INTO Game.Item (id, code, name, description, game_id) VALUES (2, "D8up3", "Voiture", "Une voiture brûlée mais encore chaude à l'entrée du site.", 1);
-- Related to AnyPlayer_1 (only)
INSERT INTO Game.Item (id, code, name, description, game_id) VALUES (3, "80u33", "Bouée Licorne 🦄", "Une réponse, une damnation, un pouvoir incommensurable.", 1);

---------- Memories
-- Memories for AnyPlayer_1
INSERT INTO Game.Memory (id, base_file_path, blurred_file_path, blurred_timestamp, intensity, mood_color, item_id, player_id) VALUES (1, "mem001.html", NULL, NULL, 25, "ffff00", 1, 1);
INSERT INTO Game.Memory (id, base_file_path, blurred_file_path, blurred_timestamp, intensity, mood_color, item_id, player_id) VALUES (2, "mem002.html", "blurred_mem002.html", NULL, 75, "ff0000", 2, 1);
INSERT INTO Game.Memory (id, base_file_path, blurred_file_path, blurred_timestamp, intensity, mood_color, item_id, player_id) VALUES (3, "mem003.html", NULL, NULL, 5, "000000", 3, 1);
-- Memories for AnyPlayer_2 (memory shared on item 2)
INSERT INTO Game.Memory (id, base_file_path, blurred_file_path, blurred_timestamp, intensity, mood_color, item_id, player_id) VALUES (4, "mem004.html", NULL, NULL, 13, "ff0000", 2, 2);