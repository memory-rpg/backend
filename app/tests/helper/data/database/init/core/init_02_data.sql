-- Clean data
DELETE FROM Core.User;

-- Base test data : test users are being populated in link with Game schema to create interesting test cases

-- Admin user           : Core = All access                 | Game = All access
INSERT INTO Core.User (id, user_name, user_pwd) VALUES (1, "Admin", "$2b$12$9kLHJGhm5tEhalkJmoDGDOuPrnlsGlZC73GNa9WiP5tzIQL9yem3.");
-- Guest user           : Core = Restricted to themselves   | Game = All access or no access, cannot be a player
INSERT INTO Core.User (id, user_name, user_pwd) VALUES (2, "Guest", "$2b$12$3xfDh4u3mbMTHgjC91vN/ucEWCi4jfkTgtHWYqjBOJ8EIHliMKSRC");
-- Random player user   : Core = Restricted to themselves   | Game = Restricted to their own elements => used to have all kind of elements assigned
INSERT INTO Core.User (id, user_name, user_pwd) VALUES (3, "AnyPlayer_1", "$2b$12$3xfDh4u3mbMTHgjC91vN/ucEWCi4jfkTgtHWYqjBOJ8EIHliMKSRC");
-- Random player user   : Core = Restricted to themselves   | Game = Restricted to their own elements => used to have all kind of elements assigned
INSERT INTO Core.User (id, user_name, user_pwd) VALUES (4, "AnyPlayer_2", "$2b$12$3xfDh4u3mbMTHgjC91vN/ucEWCi4jfkTgtHWYqjBOJ8EIHliMKSRC");
-- Random player user   : Core = Restricted to themselves   | Game = Restricted to their own elements => used to have NO kind of elements assigned
INSERT INTO Core.User (id, user_name, user_pwd) VALUES (5, "AnyPlayer_3", "$2b$12$3xfDh4u3mbMTHgjC91vN/ucEWCi4jfkTgtHWYqjBOJ8EIHliMKSRC");
-- Random player user   : Core = Restricted to themselves   | Game = All access
INSERT INTO Core.User (id, user_name, user_pwd) VALUES (6, "SuperGM", "$2b$12$3xfDh4u3mbMTHgjC91vN/ucEWCi4jfkTgtHWYqjBOJ8EIHliMKSRC");
-- Not a player user   : Core = Restricted to themselves    | Game = No access
INSERT INTO Core.User (id, user_name, user_pwd) VALUES (7, "NoPlayer", "$2b$12$3xfDh4u3mbMTHgjC91vN/ucEWCi4jfkTgtHWYqjBOJ8EIHliMKSRC");

