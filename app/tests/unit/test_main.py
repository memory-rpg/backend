"""Test main / public APIs : only APIs with no sub-calls."""
from app.config.config import get_base_settings
from app.main import app
from fastapi.testclient import TestClient

client = TestClient(app)


class TestReadRoot():
    def test_read_root(self):
        # With and Given
        response = client.get("/")
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response_json["app_name"] == get_base_settings().app_name
        assert response_json["app_version"] == get_base_settings().app_version
        assert len(response_json.keys()) == len(get_base_settings().__dict__.keys())


class TestReadFavicon():
    def test_read_favicon(self):
        # With and Given
        response = client.get("/favicon")

        # Assert
        assert response.status_code == 200
        # Exact type depends on OS and browser : 'image/vnd.microsoft.icon' or 'image/x-icon'
        assert response.headers["content-type"].endswith("icon")
        assert int(response.headers["content-length"]) > 0
