from app.core.entity.pymodel.player import PlayerStatus


class TestPlayerStatus_ClarityCoeff():
    def test_blur_coeff(self):
        # Given
        coeff_good = PlayerStatus.GOOD.blur_coeff
        coeff_injured = PlayerStatus.INJURED.blur_coeff
        coeff_bad = PlayerStatus.BAD.blur_coeff
        # Assert
        assert 0 <= coeff_good < coeff_injured < coeff_bad <= 1
