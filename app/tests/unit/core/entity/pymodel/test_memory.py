"""Most use cases are covered by Integration Tests : here we test some "limit" cases."""
import pytest
from app.core.datasource.database.connector import SessionLocal
from app.core.entity.crud.memory import (get_specific_filters_for_memory,
                                         update_memory)
from app.core.entity.pymodel.memory import Memory


class TestUpdateMemory():
    def test_update_memory(self):
        # With
        session = SessionLocal()
        memory = Memory(id=1, base_file_path="zz", item_id=1, player_id=1)

        # Given
        status = update_memory(session, memory)

        # Assert
        assert status == True

    def test_update_memory_failure(self):
        # With
        session = SessionLocal()
        memory = Memory(id=-1, base_file_path="zz", item_id=1, player_id=1)

        # Given
        status = update_memory(session, memory)

        # Assert
        assert status == False


class TestSpecificFilterForeMemory():
    def test_get_specific_filters_for_memory(self):
        # With
        session = SessionLocal()
        game_id = 1
        player_id = 2  # Guest -> NPC in Game 1

        # Given -> Assert PermissionError
        with pytest.raises(PermissionError) as _:
            _ = get_specific_filters_for_memory(session, game_id, player_id)
