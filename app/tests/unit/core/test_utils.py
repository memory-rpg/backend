from app.core.utils import calc_colors_proximity


class TestCalcColorsProximity():
    def test_calc_colors_proximity(self):
        # With
        color_1 = "00ee00"
        color_2 = "ffeeff"

        # Given
        proximity_score_1 = calc_colors_proximity(color_1, color_2)
        proximity_score_2 = calc_colors_proximity(color_2, color_1)

        # Assert
        assert proximity_score_1 == proximity_score_2
        assert proximity_score_1 == 33.33

    def test_calc_colors_proximity_same(self):
        # With
        color_black = "ffffff"
        color_white = "000000"
        color_dark_green = "008400"

        # Given
        proximity_score_same_black = calc_colors_proximity(color_black, color_black)
        proximity_score_same_white = calc_colors_proximity(color_white, color_white)
        proximity_score_same_dark_green = calc_colors_proximity(color_dark_green, color_dark_green)

        # Assert
        assert proximity_score_same_black == 100.0
        assert proximity_score_same_white == 100.0
        assert proximity_score_same_dark_green == 100.0

    def test_calc_colors_proximity_opposites(self):
        # With
        color_black = "ffffff"
        color_white = "000000"
        color_green = "00ff00"
        color_magenta = "ff00ff"

        # Given
        proximity_score_opposites_b_w = calc_colors_proximity(color_white, color_black)
        proximity_score_opposites_g_m = calc_colors_proximity(color_green, color_magenta)

        # Assert
        assert proximity_score_opposites_b_w == 0.0
        assert proximity_score_opposites_g_m == 0.0
