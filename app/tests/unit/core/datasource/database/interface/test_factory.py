

import pytest
from app.core.datasource.database.interface.common import (ManagedDatabase)
from app.core.datasource.database.interface.factory import \
    database_helper_factory
from app.core.datasource.database.interface.sqlite import SQLiteHelper


class TestDatabaseHelperFactory():
    def test_database_helper_factory(self):
        # With and Given
        db_sqlite = database_helper_factory(ManagedDatabase.SQLITE)

        # Assert
        assert db_sqlite._database_type == ManagedDatabase.SQLITE
        assert type(db_sqlite) == SQLiteHelper

    def test_database_helper_factory_exceptions(self):
        # With and Given : Assert Not Implemented
        with pytest.raises(NotImplementedError) as e1:
            _ = database_helper_factory("another_thing")
        assert "Support of another_thing is not implemented yet." in str(e1)
