import os.path

import pytest
from app.config.config import get_database_settings
from app.core.datasource.database.interface.sqlite import SQLiteHelper
from mock import patch
from sqlalchemy.exc import OperationalError


@pytest.mark.usefixtures("remove_test_database")
@patch("app.core.datasource.database.interface.sqlite.DatabaseHelper.execute_scripts")
class TestInitDatabaseConnection():
    def test_init_database_connection(self, mock_execute_scripts):
        # Given
        sqlite_helper = SQLiteHelper()

        # With
        sqlite_helper.init_database_connection()

        # Assert
        assert mock_execute_scripts.call_count == 2

    def test_init_database_connection_not_init(self, mock_execute_scripts):
        # Given
        sqlite_helper = SQLiteHelper()

        # With
        sqlite_helper.init_database_connection(False)

        # Assert
        assert mock_execute_scripts.call_count == 0

    def test_init_database_connection_force_init(self, mock_execute_scripts):
        # Given
        sqlite_helper = SQLiteHelper()

        # With
        sqlite_helper.init_database_connection(False, True)

        # Assert
        assert mock_execute_scripts.call_count == 2

    def test_init_database_connection_OperationalError(self, mock_execute_scripts):
        """Assert that in case of database script execution failure, the created database is being removed."""
        # Given
        # Init. file to test removal in exception.
        # It is needed because the database seems to be really created after the first statetement is being executed.
        # In our case, we mock the script execution so we never actually "validate" the database.
        open(get_database_settings().database_url, 'a').close()
        sqlite_helper = SQLiteHelper()
        mock_execute_scripts.side_effect = OperationalError(None, None, None)

        # With -> Assert OperationalError
        with patch("os.path.isfile", return_value=True):
            with pytest.raises(OperationalError) as _:
                _ = sqlite_helper.init_database_connection(False, True)

        assert not os.path.isfile(sqlite_helper._db_url)  # Ensure file has been removed
