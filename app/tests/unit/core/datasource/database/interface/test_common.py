import pytest
from app.core.datasource.database.connector import engine
from app.core.datasource.database.interface.common import (DatabaseHelper,
                                                           ManagedDatabase)
from mock import patch


class TestManagedDatabaseFactory():
    def test_get_enum_from_value(self):
        # With and Given
        enum_res = ManagedDatabase.get_enum_from_value("sqlite")
        # Assert
        assert enum_res == ManagedDatabase.SQLITE

    def test_get_enum_from_value_exception(self):
        # With and Given : Assert Not Implemented
        with pytest.raises(ValueError) as e1:
            _ = ManagedDatabase.get_enum_from_value("another_thing")
        assert "another_thing if not a supported database type." in str(e1)


@pytest.mark.usefixtures("remove_test_database")
class TestDatabaseHelper_ExecuteScript():
    @patch("app.core.datasource.database.connector.engine.execute")
    def test_execute_script(self, mock_engine_execute):
        # Given
        script_file = "app/tests/helper/data/database/limit_scripts/01_sql_demo.sql"

        # With
        DatabaseHelper.execute_script(script_file, engine, True)

        # Assert
        assert mock_engine_execute.call_count == 2

    @patch("app.core.datasource.database.connector.engine.execute")
    def test_execute_script(self, mock_engine_execute):
        # Given
        script_file = "app/tests/helper/data/database/limit_scripts/02_sql_demo.sQl"

        # With
        DatabaseHelper.execute_script(script_file, engine, True)

        # Assert
        assert mock_engine_execute.call_count == 5

    @patch("app.core.datasource.database.interface.sqlite.DatabaseHelper.execute_script")
    def test_execute_scripts(self, mock_execute_script):
        # Given
        script_folder = "app/tests/helper/data/database/limit_scripts/"

        # With
        DatabaseHelper.execute_scripts(script_folder, engine)

        # Assert
        assert mock_execute_script.call_count == 3
