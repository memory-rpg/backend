from app.core.domain.behavior.memory_manager import calculate_clarity_value
from app.core.entity.pymodel.memory import Memory
from app.core.entity.pymodel.player import PlayerStatus


class TestCalculateClarityValue():

    def test_calculate_clarity_value(self):
        # Given
        memory = Memory(id=1,
                        base_file_path="zz",
                        item_id=1,
                        player_id=1,
                        intensity=50,
                        mood_color="7F7F7F")
        player_status = PlayerStatus.INJURED
        player_mood_color = "ffffff"

        # With
        clarity_value = calculate_clarity_value(memory,
                                                player_status,
                                                player_mood_color)

        # Assert
        # We check an interval as we do not want to stick to much with the internal behavior.
        # Overall, average values (mood color, intensity, player status) should generate an average result.
        assert 40 < clarity_value < 50

    def test_calculate_clarity_value_perfect(self):
        # Given
        memory = Memory(id=1,
                        base_file_path="zz",
                        item_id=1,
                        player_id=1,
                        intensity=100,
                        mood_color="7F7F7F")
        player_status = PlayerStatus.GOOD
        player_mood_color = "7F7F7F"

        # With
        clarity_value = calculate_clarity_value(memory,
                                                player_status,
                                                player_mood_color)

        # Assert
        assert clarity_value == 100

    def test_calculate_clarity_value_noplayerstatus(self):
        # Given
        memory = Memory(id=1,
                        base_file_path="zz",
                        item_id=1,
                        player_id=1,
                        intensity=50,
                        mood_color="7F7F7F")
        player_status = None
        player_mood_color = "ffffff"

        # With
        clarity_value = calculate_clarity_value(memory,
                                                player_status,
                                                player_mood_color)

        # Assert
        # We check an interval as we do not want to stick to much with the internal behavior.
        # Overall, average values (mood color, intensity, player status) should generate an average result.
        assert 55 < clarity_value < 65

    def test_calculate_clarity_value_nomood(self):
        # Given
        memory = Memory(id=1,
                        base_file_path="zz",
                        item_id=1,
                        player_id=1,
                        intensity=50,
                        mood_color="7F7F7F")
        player_status = PlayerStatus.INJURED
        player_mood_color = None

        # With
        clarity_value = calculate_clarity_value(memory,
                                                player_status,
                                                player_mood_color)

        # Assert
        # We check an interval as we do not want to stick to much with the internal behavior.
        # Overall, average values (mood color, intensity, player status) should generate an average result.
        assert clarity_value == 52.5
