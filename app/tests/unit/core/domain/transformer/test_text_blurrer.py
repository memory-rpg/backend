from app.core.domain.transformer.text_blurrer import blur_text_html, blur_words

NEVER_BLURRED = "NEVER HIDDEN WORDS"
ALWAYS_BLURRED = "CANNOT BE SEEN"
ALWAYS_BLURRED_REPL = "██████ ██ ████"


class TestBlurHTMLText():

    def test_blur_text_html(self):
        # Given
        blur_percentage = 50
        with open(("app/tests/helper/data/memory/memory_basic.html"), encoding="UTF-8") as html_file:
            # With
            blurred_text = blur_text_html(html_file, blur_percentage)

            # Assert
            # Management of specific tags del & mark
            assert blurred_text.count(NEVER_BLURRED) == 2
            assert blurred_text.count(ALWAYS_BLURRED) == 0
            assert blurred_text.count(ALWAYS_BLURRED_REPL) >= 3

            # Management of spaces : ensure spaces are kept between various tags
            assert blurred_text.count("visible"+NEVER_BLURRED) == 0 and blurred_text.count("███████"+NEVER_BLURRED) == 0
            assert blurred_text.count(ALWAYS_BLURRED_REPL+NEVER_BLURRED) == 1

    def test_blur_text_html_nobody(self):
        # Given
        blur_percentage = 10
        with open(("app/tests/helper/data/memory/memory_basic_nobody.html"), encoding="UTF-8") as html_file:
            # With
            blurred_text = blur_text_html(html_file, blur_percentage)

            # Assert
            # Some blur but tags did not worked (because plain text logic)
            assert blurred_text.count("CANNOT") >= 1
            assert blurred_text.count("█") >= 1


class TestBlurWords():
    def test_blur_words_100(self) -> str:
        # Given 100% chance to blur -> With
        blurred_words = blur_words(ALWAYS_BLURRED, 100)

        # Assert
        assert blurred_words == ALWAYS_BLURRED_REPL

    def test_blur_words_0(self) -> str:
        # Given 0% chance to blur -> With
        blurred_words = blur_words(NEVER_BLURRED, 0)

        # Assert
        assert blurred_words == NEVER_BLURRED

    def test_blur_words_50(self) -> str:
        """Check that with a given probability, some words are blurred and others are not.

        This test has some random in it so it could theorically fails.
        """
        # Given
        blur_percentage = 50
        list_of_words = ["a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a"]
        # With
        blurred_list_of_words = []
        for word in list_of_words:
            blurred_list_of_words.append(blur_words(word, blur_percentage))

        # Assert
        assert blurred_list_of_words.count("a") > 0
        assert blurred_list_of_words.count("█") > 0
