from time import sleep

import pytest
from app.core.api.security.authentication import (create_access_token,
                                                  validate_jwt_token)
from jose import ExpiredSignatureError, JWTError


class TestAuthenticationValidateJwtToken():
    def test_validate_jwt_token(self):
        # Given
        username = "super_username"
        token = create_access_token(data={"sub": username})

        # With
        token_data = validate_jwt_token(token)

        # Assert
        assert token_data.username == username

    def test_validate_jwt_token_jwt_error(self):
        # Given
        token_no_username = create_access_token(data={})

        # With -> Assert
        with pytest.raises(JWTError) as e:
            _ = validate_jwt_token(token_no_username)

    def test_validate_jwt_token_expiration(self):
        """Check that token expiration is throwing an error."""
        # Given
        username = "super_username"
        validity_duration = 3  # 3 seconds
        token = create_access_token(data={"sub": username}, expires_delay_in_sec=validity_duration)

        # With -> Assert no error
        _ = validate_jwt_token(token)

        # With -> Assert error
        sleep(validity_duration + 2)
        with pytest.raises(ExpiredSignatureError) as _:
            _ = validate_jwt_token(token)
