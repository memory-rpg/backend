
import pytest
from app.core.api.security.authentication import create_access_token
from app.core.datasource.database.connector import init_engine
from app.dependencies import get_current_user, get_db_session
from app.main import app
from app.tests.helper.fixtures.authentication_helper import \
    create_token_for_user_via_api
from fastapi import HTTPException
from fastapi.testclient import TestClient
from sqlalchemy.orm import sessionmaker

client = TestClient(app)

token_user_admin = create_token_for_user_via_api(client=client, username="Admin")


class TestDependenciesGetDbSession():
    def test_get_db_session(self):
        # Given & With
        session = get_db_session()

        # Assert
        assert session is not None


class TestDependenciesGetCurrentUser():
    def test_get_current_user(self):
        # Given
        engine = init_engine()
        SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

        # With
        user = get_current_user(token_user_admin, SessionLocal())

        # Assert
        assert user.user_name == "Admin"
        assert user.id == 1

    def test_get_current_user_wrong_username(self):
        # Given
        engine = init_engine()
        SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
        valid_token_invalid_username = create_access_token(data={"sub": "_not_even_remotely_a_real_username_"})

        # With -> Assert exception
        with pytest.raises(HTTPException) as e:
            _ = get_current_user(valid_token_invalid_username, SessionLocal())
