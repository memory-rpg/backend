"""Test main / public APIs : token generation control."""
from app.main import app
from fastapi.testclient import TestClient

client = TestClient(app)


class TestLoginForAccessToken():
    def test_login_for_access_token(self):
        # With and Given
        response = client.post("/token",
                               data={"grant_type": "password", "username": "Admin", "password": "_ch4ng3_M3_"})
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert "access_token" in response_json
        assert response_json["token_type"] == "bearer"

    def test_login_for_access_token_wrong_user(self):
        # With and Given (username being "NotAdmin" instead of "Admin")
        response = client.post("/token",
                               data={"grant_type": "password", "username": "NotAdmin", "password": "_ch4ng3_M3_"})
        response_json = response.json()

        # Assert
        assert response.status_code == 401
        assert response_json["detail"] == "Incorrect username or password"

    def test_login_for_access_token_wrong_password(self):
        # With and Given (password contains a 'm' instead of 'M')
        response = client.post("/token",
                               data={"grant_type": "password", "username": "Admin", "password": "_ch4ng3_m3_"})
        response_json = response.json()

        # Assert
        assert response.status_code == 401
        assert response_json["detail"] == "Incorrect username or password"
