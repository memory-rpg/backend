"""Test all `memories/` related endpoints.

This will generate a lot of integration tests as we expect to mock as less as possible in these tests.
"""

import re

from app.main import app
from app.tests.helper.fixtures.authentication_helper import (
    create_token_for_user_via_api, get_header_with_token)
from fastapi.testclient import TestClient

client = TestClient(app)

# We expected all called API to be public
header_with_token_supergm = get_header_with_token(create_token_for_user_via_api(client,
                                                                                "SuperGM"))
header_with_token_anyplayer1 = get_header_with_token(create_token_for_user_via_api(client,
                                                                                   "AnyPlayer_1"))
header_with_token_anyplayer3 = get_header_with_token(create_token_for_user_via_api(client,
                                                                                   "AnyPlayer_3"))
header_with_token_noplayer = get_header_with_token(create_token_for_user_via_api(client,
                                                                                 "NoPlayer"))


class TestReadMemories():
    def test_read_memories_gm(self):
        """SuperGM must have access to all memories."""
        # With and Given
        response = client.get("/games/1/memories",
                              headers=header_with_token_supergm)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert len(response_json) == 4
        assert "id" in response_json[0]
        assert "base_file_path" in response_json[0]
        assert "item_id" in response_json[0]
        assert "player_id" in response_json[0]

    def test_read_memories_pc(self):
        """SuperGM must have access to all memories."""
        # With and Given
        response = client.get("/games/1/memories",
                              headers=header_with_token_anyplayer1)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert len(response_json) == 1

    def test_read_memories_pc_empty(self):
        """AnyPlayer_3 must have no access to memories (empty list)."""
        # With and Given
        response = client.get("/games/1/memories",
                              headers=header_with_token_anyplayer3)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert len(response_json) == 0

    def test_read_memories_401(self):
        """User not in the game must have no access to memories (401)."""
        # With and Given
        response = client.get("/games/1/memories",
                              headers=header_with_token_noplayer)
        response_json = response.json()

        # Assert
        assert response.status_code == 401
        assert response_json["detail"] == "Memories cannot be accessed by the user."


class TestReadMemoryBlurred():
    def test_read_memory_PC(self):
        # With
        game_id = 1
        memory_number = 2
        regexp = re.compile(r'[\\w█]')

        # Given
        response = client.get((f"/games/{game_id}/memories/{memory_number}?blurred_content=true"),
                              headers=header_with_token_anyplayer1)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert regexp.search(response_json)

    def test_read_memory_GM(self):
        # With
        game_id = 1
        memory_number = 2

        # Given
        response = client.get((f"/games/{game_id}/memories/{memory_number}"),
                              headers=header_with_token_supergm)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert response_json["id"] == memory_number

    def test_read_memory_404_because_filters(self):
        # With
        memory_number = 2

        # Given
        response = client.get(("/games/1/memories/" + str(memory_number)),
                              headers=header_with_token_anyplayer3)

        # Assert
        assert response.status_code == 404
        assert response.headers["content-type"] == "application/json"
        assert response.json()["detail"] == ("Memory not found with id " + str(memory_number))

    def test_read_memory_404(self):
        # With
        memory_number = -1

        # Given
        response = client.get(("/games/1/memories/" + str(memory_number)),
                              headers=header_with_token_supergm)

        # Assert
        assert response.status_code == 404
        assert response.headers["content-type"] == "application/json"
        assert response.json()["detail"] == ("Memory not found with id " + str(memory_number))

    def test_read_memory_404_no_blur_version(self):
        # With
        memory_number = 1

        # Given
        response = client.get(("/games/1/memories/" + str(memory_number) + "?blurred_content=true"),
                              headers=header_with_token_supergm)

        # Assert
        assert response.status_code == 404
        assert response.headers["content-type"] == "application/json"
        assert response.json()["detail"] == ("Not blurred memory associated to memory id " + str(memory_number))

    def test_read_memory_401(self):
        # With
        memory_number = 2

        # Given
        response = client.get(("/games/1/memories/" + str(memory_number)),
                              headers=header_with_token_noplayer)
        response_json = response.json()

        # Assert
        assert response.status_code == 401
        assert response_json["detail"] == f"Memory id {memory_number} cannot be accessed by the user."
