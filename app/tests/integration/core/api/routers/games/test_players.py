"""Test all `players/` related endpoints.

This will generate a lot of integration tests as we expect to mock as less as possible in these tests.
"""

from app.main import app
from app.tests.helper.fixtures.authentication_helper import (
    create_token_for_user_via_api, get_header_with_token)
from fastapi.testclient import TestClient

client = TestClient(app)

header_with_token_anyplayer1 = get_header_with_token(create_token_for_user_via_api(client,
                                                                                   "AnyPlayer_1"))
header_with_token_noplayer = get_header_with_token(create_token_for_user_via_api(client,
                                                                                 "NoPlayer"))


class TestReadPlayers():
    def test_read_players(self):
        # With and Given
        response = client.get("/games/1/players",
                              headers=header_with_token_anyplayer1)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert "id" in response_json[0]
        assert "character_name" in response_json[0]
        assert "game_id" in response_json[0]
        assert "user_id" in response_json[0]
        assert len(response_json) > 1

    def test_read_players_query_params_exclude_me(self):
        # With and Given
        response = client.get("/games/1/players?exclude_me=true",
                              headers=header_with_token_anyplayer1)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        for player in response_json:
            assert player["user_id"] != 3  # AnyPlayer_1 user's id

    def test_read_players_query_params_players_type(self):
        # With and Given
        response = client.get("/games/1/players?player_types=GM&player_types=Guest",
                              headers=header_with_token_anyplayer1)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert len(response_json) == 1  # Only GM

    def test_read_players_401(self):
        # With and Given
        response = client.get("/games/1/players",
                              headers=get_header_with_token("unreal_token"))
        response_json = response.json()

        # Assert
        assert response.status_code == 401
        assert response_json["detail"] == "Could not validate credentials"


class TestReadPlayer():
    def test_read_player(self):
        # With
        player_number = 1

        # Given
        response = client.get(("/games/1/players/" + str(player_number)),
                              headers=header_with_token_anyplayer1)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert response_json["id"] == player_number
        assert response_json["user_id"] == 3
        assert response_json["character_name"] == "Jean le bûcheron"

    def test_read_player_404(self):
        # With
        player_number = -1

        # Given
        response = client.get(("/games/1/players/" + str(player_number)),
                              headers=header_with_token_anyplayer1)

        # Assert
        assert response.status_code == 404
        assert response.headers["content-type"] == "application/json"
        assert response.json()["detail"] == ("Player not found with id " + str(player_number))

    def test_read_player_401(self):
        # With
        player_number = 1

        # Given
        response = client.get(("/games/1/players/" + str(player_number)),
                              headers=get_header_with_token("unreal_token"))
        response_json = response.json()

        # Assert
        assert response.status_code == 401
        assert response_json["detail"] == "Could not validate credentials"


class TestReadPlayerMe():
    def test_read_player_me(self):
        # With and Given
        response = client.get("/games/1/players/me",
                              headers=header_with_token_anyplayer1)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert response_json["id"] == 1
        assert response_json["user_id"] == 3
        assert response_json["character_name"] == "Jean le bûcheron"

    def test_read_player_me_404(self):
        # With and Given
        response = client.get("/games/1/players/me",
                              headers=header_with_token_noplayer)

        # Assert
        assert response.status_code == 404
        assert response.headers["content-type"] == "application/json"
        assert response.json()["detail"] == ("No player is associated with the user 'NoPlayer'.")

    def test_read_player_me_401(self):
        # With and  Given
        response = client.get("/games/1/players/me",
                              headers=get_header_with_token("unreal_token"))
        response_json = response.json()

        # Assert
        assert response.status_code == 401
        assert response_json["detail"] == "Could not validate credentials"
