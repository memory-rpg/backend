"""Test all `items/` related endpoints.

This will generate a lot of integration tests as we expect to mock as less as possible in these tests.
"""


from app.main import app
from app.tests.helper.fixtures.authentication_helper import (
    create_token_for_user_via_api, get_header_with_token)
from fastapi.testclient import TestClient

client = TestClient(app)

# We expected all called API to be public
header_with_token_supergm = get_header_with_token(create_token_for_user_via_api(client,
                                                                                "SuperGM"))
header_with_token_anyplayer1 = get_header_with_token(create_token_for_user_via_api(client,
                                                                                   "AnyPlayer_1"))
header_with_token_anyplayer2 = get_header_with_token(create_token_for_user_via_api(client,
                                                                                   "AnyPlayer_2"))
header_with_token_anyplayer3 = get_header_with_token(create_token_for_user_via_api(client,
                                                                                   "AnyPlayer_3"))
header_with_token_noplayer = get_header_with_token(create_token_for_user_via_api(client,
                                                                                 "NoPlayer"))


class TestReadItems():
    def test_read_items(self):
        # With and Given
        response = client.get("/games/1/items",
                              headers=header_with_token_supergm)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert "id" in response_json[0]
        assert "description" in response_json[0]
        assert "memories" not in response_json[0]

    def test_read_items_401(self):
        # With and Given
        response = client.get("/games/1/items",
                              headers=header_with_token_noplayer)
        response_json = response.json()

        # Assert
        assert response.status_code == 401
        assert response_json["detail"] == "Items cannot be accessed by the user."

    def test_read_items_401_wrong_user(self):
        # With and Given
        response = client.get("/games/1/items",
                              headers=get_header_with_token("unreal_token"))
        response_json = response.json()

        # Assert
        assert response.status_code == 401
        assert response_json["detail"] == "Could not validate credentials"


class TestReadItem():
    def test_read_item(self):
        # With
        item_number = 2

        # Given
        response = client.get(("/games/1/items/" + str(item_number)),
                              headers=header_with_token_supergm)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert response_json["id"] == item_number
        assert "memories" in response_json
        assert response_json["memories"]
        assert "id" in response_json["memories"][0]

    def test_read_item_404(self):
        # With
        item_number = -1

        # Given
        response = client.get(("/games/1/items/" + str(item_number)),
                              headers=header_with_token_supergm)

        # Assert
        assert response.status_code == 404
        assert response.headers["content-type"] == "application/json"
        assert response.json()["detail"] == ("Item not found with id " + str(item_number))

    def test_read_item_401(self):
        # With
        item_number = 2

        # Given
        response = client.get(("/games/1/items/" + str(item_number)),
                              headers=header_with_token_noplayer)
        response_json = response.json()

        # Assert
        assert response.status_code == 401
        assert response_json["detail"] == "Items cannot be accessed by the user."
