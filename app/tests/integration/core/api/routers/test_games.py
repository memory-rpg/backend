"""Test all `games/` related endpoints."""

from app.main import app
from app.tests.helper.fixtures.authentication_helper import (
    create_token_for_user_via_api, get_header_with_token)
from fastapi.testclient import TestClient

client = TestClient(app)

header_with_token_admin = get_header_with_token(create_token_for_user_via_api(client,
                                                                              "Admin"))

header_with_token_supergm = get_header_with_token(create_token_for_user_via_api(client,
                                                                                "SuperGM"))
header_with_token_anyplayer1 = get_header_with_token(create_token_for_user_via_api(client,
                                                                                   "AnyPlayer_1"))
header_with_token_anyplayer2 = get_header_with_token(create_token_for_user_via_api(client,
                                                                                   "AnyPlayer_2"))
header_with_token_anyplayer3 = get_header_with_token(create_token_for_user_via_api(client,
                                                                                   "AnyPlayer_3"))
header_with_token_noplayer = get_header_with_token(create_token_for_user_via_api(client,
                                                                                 "NoPlayer"))


class TestReadGames():
    def test_read_games(self):
        # With and Given
        response = client.get("/games",
                              headers=header_with_token_admin)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert response_json[0]["name"] == "Demo Game"
        assert response_json[0]["id"] == 1

    def test_read_games_public(self):
        # With and Given
        response = client.get("/games",
                              headers=get_header_with_token("unreal_token"))
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert len(response_json) > 0


class TestReadGame():
    def test_read_game(self):
        # With
        game_id = 1

        # Given
        response = client.get(f"/games/{game_id}",
                              headers=header_with_token_admin)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert response_json["name"] == "Demo Game"
        assert response_json["id"] == 1

    def test_read_game_404(self):
        # With
        game_id = -1

        # Given
        response = client.get(f"/games/{game_id}",
                              headers=header_with_token_admin)
        response_json = response.json()

        # Assert
        assert response.status_code == 404
        assert response_json["detail"] == f"Game not found with id {game_id}"

    def test_read_game_401(self):
        # With
        game_id = 1

        # Given
        response = client.get(f"/games/{game_id}",
                              headers=get_header_with_token("unreal_token"))
        response_json = response.json()

        # Assert
        assert response.status_code == 401
        assert response_json["detail"] == "Could not validate credentials"


class TestDiscoverMemory():
    def test_post_discover_memory_via_item(self):
        # With
        game_id = 1
        item_code = "D8up3"  # 2players memories on that item

        # Given
        response_anyplayer1 = client.post((f"/games/{game_id}/discover-memory?item_code={item_code}"),
                                          headers=header_with_token_anyplayer1)
        response_anyplayer2 = client.post((f"/games/{game_id}/discover-memory?item_code={item_code}"),
                                          headers=header_with_token_anyplayer2)
        response_json1 = response_anyplayer1.json()
        response_json2 = response_anyplayer2.json()

        # Assert
        assert response_anyplayer1.status_code == 200
        assert response_anyplayer1.headers["content-type"] == "application/json"
        assert response_anyplayer2.status_code == 200
        assert response_anyplayer2.headers["content-type"] == "application/json"
        assert response_json1 == 2
        assert response_json2 == 4

    def test_post_discover_memory_via_item_wrongcode(self):
        # With
        game_id = 1
        item_code = "notAcode"

        # Given
        response = client.post((f"/games/{game_id}/discover-memory?item_code={item_code}"),
                               headers=header_with_token_anyplayer1)

        # Assert
        assert response.status_code == 404
        assert response.headers["content-type"] == "application/json"

    def test_post_discover_memory_via_item_gm(self):
        # With
        game_id = 1
        item_code = "D8up3"  # 2players memories on that item

        # Given
        response = client.post((f"/games/{game_id}/discover-memory?item_code={item_code}"),
                               headers=header_with_token_supergm)

        # Assert
        assert response.status_code == 404
        assert response.headers["content-type"] == "application/json"

    def test_post_discover_memory_via_item_otherplayer(self):
        # With
        game_id = 1
        item_code = "D8up3"  # 2players memories on that item

        # Given
        response = client.post((f"/games/{game_id}/discover-memory?item_code={item_code}"),
                               headers=header_with_token_anyplayer3)

        # Assert
        assert response.status_code == 404
        assert response.headers["content-type"] == "application/json"

    def test_post_discover_memory_via_item_notplayer(self):
        # With
        game_id = 1
        item_code = "D8up3"  # 2players memories on that item

        # Given
        response = client.post((f"/games/{game_id}/discover-memory?item_code={item_code}"),
                               headers=header_with_token_noplayer)

        # Assert
        assert response.status_code == 401
        assert response.headers["content-type"] == "application/json"
