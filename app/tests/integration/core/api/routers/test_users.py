"""Test all `users/` related endpoints."""

from app.main import app
from app.tests.helper.fixtures.authentication_helper import (
    create_token_for_user_via_api, get_header_with_token)
from fastapi.testclient import TestClient

client = TestClient(app)

header_with_token_admin = get_header_with_token(create_token_for_user_via_api(client,
                                                                              "Admin"))


class TestReadUsersMe():
    def test_read_user_me(self):
        # With and Given
        response = client.get("/users/me",
                              headers=header_with_token_admin)
        response_json = response.json()

        # Assert
        assert response.status_code == 200
        assert response.headers["content-type"] == "application/json"
        assert response_json["user_name"] == "Admin"
        assert response_json["id"] == 1

    def test_read_user_me_401(self):
        # With and Given
        response = client.get("/users/me",
                              headers=get_header_with_token("unreal_token"))
        response_json = response.json()

        # Assert
        assert response.status_code == 401
        assert response_json["detail"] == "Could not validate credentials"
