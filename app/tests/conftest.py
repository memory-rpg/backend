import os
from os import walk

import pytest
from app.config.config import get_database_settings, get_filer_settings
from sqlalchemy.orm import close_all_sessions


@pytest.fixture(scope="function")
def remove_test_database():
    """Remove database file between 2 tests to avoid conflicts."""
    database_path = get_database_settings().database_url
    if os.path.isfile(database_path):
        os.remove(database_path)


@pytest.fixture(scope="session", autouse=True)
def setup_and_teardown():
    """Execute Setup and Teardown for the Tests sessions.

    For now, only the teardown part is being used to remove temporary files generated during tests session. If you \
    need to add any cleanup action to ensure tests are run against the same base each time, please add actions below.

    Yields:
        int: `1` as a dummy value to split setup/teardown sections.
    """
    # SETUP
    # We need a yield to split setup from teardown section ... but we have nothing to yield :^)
    yield 1

    # TEARDOWN
    # Close in-memory database sessions to allows SQlite database file deletion
    close_all_sessions()
    # Remove SQLite temporary files
    database_path = get_database_settings().database_scripts_path
    _, _, filenames = next(walk(database_path), (None, None, []))
    _ = [os.remove(os.path.join(database_path, file)) for file in filenames if file.lower().endswith(".db")]
    # Remove "blurred memory file" generated during tests
    os.remove(os.path.join(get_filer_settings().memories_content_path, "blurred_mem004.html"))
