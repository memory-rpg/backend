import logging
import os
from functools import lru_cache
from typing import Dict

from pydantic import BaseSettings

logger = logging.getLogger(__name__)

GLOBAL_ENV_VARS = os.getenv("GLOBAL_ENV_VARS", "app/config/.env")
logger.info("Application settings : loaded from %s",
            GLOBAL_ENV_VARS)


class Settings(BaseSettings):
    app_name: str = "Memory RPG API"
    app_version: str
    url_swagger: str = "/docs"
    dev_email: str

    class Config:
        env_file = GLOBAL_ENV_VARS


@lru_cache()
def get_base_settings():
    return Settings()


class SettingsDatabase(BaseSettings):
    database_type: str
    database_scripts_path: str
    database_url: str
    database_connect_args: Dict

    class Config:
        env_file = GLOBAL_ENV_VARS


@lru_cache()
def get_database_settings():
    return SettingsDatabase()


class SettingsFiler(BaseSettings):
    static_content_path: str
    memories_content_path: str
    temp_path: str

    class Config:
        env_file = GLOBAL_ENV_VARS


@lru_cache()
def get_filer_settings():
    return SettingsFiler()
