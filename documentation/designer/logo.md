# Logo

The logo have 2 versions : a light and warn one and a dark one. Both versions have there grey levels versions.

## Light versions

![Light version of the logo](.img/logo/memory_rpg_logo_light.png)
![Light version of the logo with grey levels](.img/logo/memory_rpg_logo_light_niveaux_gris.png)

## Dark versions

![Dark version of the logo](.img/logo/memory_rpg_logo_dark.png)
![Dark version of the logo with grey levels](.img/logo/memory_rpg_logo_dark_niveaux_gris.png)

## Other files

You can find :
- In folder [logo/](./img/logo/) : all logo versions.
- In folder [icon/](./img/icon/) : all icons versions (generated from light version of logo from [favicon.io](https://favicon.io/favicon-converter/) website).
  - favicon (16x16, 32x32, 64x64).
  - smartphone version (android, apple).
- The [source file](.img/memory_rpg_logo.xcf) in GIMP format but unfortunately all in grey levels due to a manipulation error...)

> ___
> ↩️ Browse back to [summary](./INDEX.md) page.
