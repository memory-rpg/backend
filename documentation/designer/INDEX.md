# Designer documentation

## Current section documentation files

| File                   | Description                              |
| :--------------------- | :--------------------------------------- |
| [../](../INDEX.md)     | Back to previous documentation level.    |
| [INDEX.md](./INDEX.md) | Current file.                            |
| [logo.md](./logo.md)   | Logo usage, versions, etc. presentation. |


## Sub-sections documentation

>___
> This is the end of the section : go back to [previous section](../INDEX.md). 🔚
