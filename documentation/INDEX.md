# Application documentation


## Current section documentation files

Documentation in that folder: 
| File                             | Description                                          |
| :------------------------------- | :--------------------------------------------------- |
| [../](../README.md)              | Back to previous documentation level.                |
| [INDEX.md](./INDEX.md)           | Current file.                                        |
| [CONTRIBUTE.md](./CONTRIBUTE.md) | Learn how-to make this place a better documentation. |

## Sub-sections documentation
| Folder                                  | Description                                                                                                                                  |
| :-------------------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------- |
| [Developer](./developer/INDEX.md)       | Everything regarding environment setup, coding rules, snippets, local utilities, ...                                                         |
| [Domain](./domain/INDEX.md)             | Presentation and/or analysis of code mechanism regarding some features. It can go to very "use case" oriented to very technical explanation. |
| [Architecture](./architecture/INDEX.md) | Overview of the application security (general mechanims, flows, security, ...).                                                              |
| [Designer](./designer/INDEX.md)         | Various things about the logo.                                                                                                               |

>___
> This is the end of the section : go back to [previous section](../README.md). 🔚