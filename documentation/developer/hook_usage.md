# Hooks usage

[Git hooks](https://git-scm.com/docs/githooks) allows to trigger actions on specific `git` actions. Their usage is at the developer discretion as they are to be deployed in the unversioned `.git/hooks/` folder.

- [Hooks usage](#hooks-usage)
  - [Projects hooks](#projects-hooks)
  - [How-to use them](#how-to-use-them)
  - [Contribute](#contribute)

## Projects hooks

| Hook                           | Description                                                                                                                                                         | Reference doc                                                         |
| :----------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------ | :-------------------------------------------------------------------- |
| [pre-push](pre-push)           | Execute tests before pushing code. It is meant to be an helper to avoid pushing code with lower quality than expected. _This is not blocking by default_.           | [Git documentation](https://git-scm.com/docs/githooks#_pre_push)      |
| [post-checkout](post-checkout) | When checkouting the `main`, it will attempt to `git fetch`, `git pull` and install all python packages. It is meant to ensure `main` is as up-to-date as possible. | [Git documentation](https://git-scm.com/docs/githooks#_post_checkout) |
| [pre-commit](pre-commit)       | Before commiting, it will attempt to clean the unused import with `pycln app/` command.                                                                             | [Git documentation](https://git-scm.com/docs/githooks#_pre_commit)    |

>___
> 📝 **Info**
> ___
> In order to work with VSCode+Python integration, hooks must starts by activating your virtual environment with a command such as `source .venv/Scripts/activate
`. You may have to update it for yoru local environment.
> ___


## How-to use them

Copy all hooks from [current hooks folder](./) to [ git hooks folder](../../../.git/hooks/) (`.git/hooks/`).

This folder allows to version and share hooks with other developers.

## Contribute

Hooks have to answer a project and developer needs : it would be better to adapt them than desactivating them. Do not hesitate to share or ask for an improvment on hooks !

When updating a hook remember that :
- Testing it is a bit tricky :)
- You should update it in your `.git` folder to use it **and** add it in the current folder to share it with others.

> ___
> ↩️ Browse back to [summary](./INDEX.md) page.