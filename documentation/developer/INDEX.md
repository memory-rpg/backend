# Developer documentation

## Current section documentation files

| File                                      | Description                                                                            |
| :---------------------------------------- | :------------------------------------------------------------------------------------- |
| [../](../INDEX.md)                        | Back to previous documentation level.                                                  |
| [INDEX.md](./INDEX.md)                    | Current file.                                                                          |
| [Developer Guide](./developer_guide.md)   | Starting documentation for the developer : local environment setup, base commands, ... |
| [Coding guide](./coding_guide.md)         | Coding rules, Gitflow                                                                  |
| [How to use git hooks ?](./hook_usage.md) | Review of (useful ?) git hooks to help you on local environment                        |


## Sub-sections documentation

>___
> This is the end of the section : go back to [previous section](../INDEX.md). 🔚
