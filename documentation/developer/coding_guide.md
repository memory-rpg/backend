# Coding guide

Make your bed, air rooms for 10 minutes daily and try to clean code !

- [Coding guide](#coding-guide)
  - [Coding rules](#coding-rules)
    - [Code coverage](#code-coverage)
      - [Write a test](#write-a-test)
      - [Exceptions (# no cover)](#exceptions--no-cover)
    - [Security](#security)
  - [Gitflow](#gitflow)
  - [External libraries](#external-libraries)

## Coding rules

1. Follows (most of) pylint recommendations.
2. Sign your methods : `typing` is your best friend.
3. Document your methods even if they looks trivial at the moment (your yourself from 2 years in future is thanking you).
4. Ensure your methods docstring follows [Google style](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html) logic.
5. Cover your code with tests : the next guy is going to put some regression in your beautiful code if you don't. (You are probably the next guy).

### Code coverage

Overall, we expect the code to be 100% covered by tests. It could be time consuming, so here are the general rules to achieve that :

- **Integration Tests** for most of the code :
  - Theses will access an entry point (most likely an API Endpoint) and go over your code.
- **Unit Tests** for most complex methods and/or critical/key methods :
  - *Complex methods* : Covering code with a lot of branching faster, will help others to not integrate regressions in hard to understand methods, allows to focus more on limit-cases testing.
  - *Critical/Key methods* : You do not want to deliver an application with a broken key feature. So this will ensure their are always tested.
- Exclude some code from coverage in case you cannot cover it or there is no point to do so. By example, an abstract method which only has the code `pass` may not be worth a test. **It should be used at the edge**.

> ❕ Do not forget to add some docstring or comments in your most complex tests !

📃One may read more documentation on [Read the Docs](https://coverage.readthedocs.io/en/6.2/).

#### Write a test

Tests are stored under `app/tests/` and have the following structure :

```txt
tests/
|_ helper/        # Contains data & helper methods to run tests
|_ integration/   # Contains integration tests, i.e. which are executing several parts of the code (e.g. an API call).
|_ unit/          # Contains unit tests, i.e. which are executing small code unit (functions or modules scoped)
|_ conftest.py    # Fixtures implementation re-usable in whole tests/ folder
|_ pytest.ini     # Configuration of pytest (environment variables, marks, etc.)
```

Few rules :

- Tests must be written under `unit/` or `integration/` folder.
- Tests must be written at the same level of the tree of the code being tested. If you want to write a test for a function of `app/core/supermodule.py` then it will look like :

  ```txt
  app/
  |_ core/
    |_ supermodule.py
  |_ tests/
    |_ integration/
    |_ unit/
      |_ code/
        |_ test_supermodule.py
  ```
- Test files must be prefixed by `test_` and end with `.py` extension to be discovered.
- Test functions  must be prefixed by `test_` to be discovered :
  - `test_<function_name>_<tests_specificity>` is a good way to easily determine which test is failing in logs. By example, `def test_read_item_memories_401()` is testing function `read_item_memories()` and we do want to test the HTTP code `401` behavior.
  - Tests of a same function (but for various behaviors) should be grouped in a class named `Test<FunctionName>`. By example, `class TestReadItemMemories()`.

Should I write an unit or integration test ?

Good question, no perfect answer : it is all about the criticity of the code, the time you have, the complexity of the module, etc.

**Unit tests**

- ✔️are good to have a very quick and sharp detection of an issue.
- ✔️allows to tests a lot of use cases, limit cases, ... on a single part of the code. If you want to test your whole branching logic of your code, unit tests are mandatory.
- ❌will need more work?
- ❌can be **very** time consuming when it comes to mock some data / functions / etc.

**Integration tests**
- ✔️are good to ensure that all code parts are nicely working together.
- ✔️will cover more code at once.
- ❌will be less precise on the _real_ possible uses cases.

The rule of thumb could be :

| Code vs Test                           | Unit | Integ |
| :------------------------------------- | :--- | :---- |
| Very simple code                       | ✔️    | ✔️     |
| Critical code                          | ✔️    |       |
| Code with lot of branching             | ✔️    |       |
| Code with lot of data sources / target |      | ✔️     |

#### Exceptions (# no cover)

Here is recap of the `# pragma: no cover` and why it is being used :

| File/Class(/Method)/Function                                | Code part             | Justification                                                                                                                                                                                                               |
| :---------------------------------------------------------- | :-------------------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `sqlite.py > SQLiteHelper > init_database_connection()`     | `if os.path.isfile()` | Coverage miss that test but it is actually tested specifically by `test_sqlite.py > TestInitDatabaseConnection > test_init_database_connection_OperationalError()`. It can be validated by running tests and checking logs. |
| `sqlite.py>  listener_connect()`                            | Whole method.         | This method is actually covered through `SQLiteHelper.init_database_connection()` tests. It is kind of undetected, probably because it is an event.                                                                         |
| `common.py > DatabaseHelper() > init_database_connection()` | Whole method.         | Class is abstract and should not be instanciated.                                                                                                                                                                           |

### Security

Regarding code security, we are using [bandit](https://bandit.readthedocs.io/en/latest/config.html) for checks. Code security is being described all over the internet, but here few rules :

- `MEDIUM` and `HIGH` issues are to be managed.
- `LOW` issues wont block the integration but it would be appreciated if one do regular take on them : having no security issue is good.
- Exclude some precise part of the code is possible with the `#nosec` instruction (see [example](https://bandit.readthedocs.io/en/latest/config.html#suppressing-individual-lines)).
- No `confidence` level has been set for now : this may change in future.

> Security being a special case, downloaded bandit package is always the `latest`.

## Gitflow

This repository follows a `main` mono-branch logic.

A standard flow would be :

1. Get assigned a task/issue.
2. Create a branch named `feature/<issue_number>` or `fix/<issue_number>` (e.g. `feature/103` is a branch to implement the feature ticket #103) from the `main` branch.
3. Implement your magic (and follows [coding rules](#coding-rules)).
   1. Commit frequently to allows other to help you (avoir losing your changes in case of PC crash #youknowthefeeling).
   2. Indicate the issue number in the commit message (e.g. `#457 Added magical unicorn in the api response header : does nothing but stylish.`)
4. Update the application version by following [Semantic Versioning 2.0.0](https://semver.org/) recommandations.
5. Ask for a `Merge Request` :
   1. ✔️ Set the issue number in the title of the `Merge Request`.
   2. ✔️ Ensure tests are all good.
   3. ✔️ Ask a reviewer to provide you some feedback (the best moment for everyone to learn).
   4. ✔️ Improve your change.
   5. > You can easily set your `Merge Request` as `Draft` to prepare it, share it with other developers, execute the pipeline on it ... while avoiding it to be merged by mistake. See [Draft merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/drafts.html).
6. Merge it to `main` :
   - Remove your feature/fix branch.
   - Squash commits.

## External libraries

When using an external library, one should :

- Add it in requirements :
  - requirements.txt : all packages needed to run the application (e.g. : fastapi, pandas, ...).
  - dev-requirements.txt : all packages needed (or very convinient) for the developer setup (e.g. : tests libraries).
- Fix the version of the package to make builds reproductibles.
  - `mypackage==x.y.z`
  - Do not use latest, ranges, ...
- Check licence and usage conditions (avoid black-boxes and paying libraries).

> For now, we use only libraries from pypi. If you want to use other feeds, one should document it clearly for other developers.

> ___
> ↩️ Browse back to [summary](./INDEX.md) page.