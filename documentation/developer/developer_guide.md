# Developer Guide

That documentation guide any person willing to work on the project with other related documentations.

- [Developer Guide](#developer-guide)
  - [Installation](#installation)
    - [Pre-requisites](#pre-requisites)
    - [Run the application](#run-the-application)
    - [Code checks](#code-checks)
      - [Manual checks](#manual-checks)
      - [Git hooks](#git-hooks)
      - [Pipelines](#pipelines)
  - [VS Code Setup](#vs-code-setup)
    - [Recommended plugins](#recommended-plugins)
    - [Preferences Configuration](#preferences-configuration)
  - [Code contribution](#code-contribution)
  - [More](#more)
    - [Known issues](#known-issues)
      - [Cannot open VS Code User Preferences as JSON](#cannot-open-vs-code-user-preferences-as-json)
      - [FileNotFoundError while running pytests](#filenotfounderror-while-running-pytests)
    - [Resources](#resources)

## Installation

### Pre-requisites

**Mandatory** :

- Installed [Python 3.10](https://www.python.org/downloads/release/python-3100/).
- Cloned the [memory-rpg repository](https://framagit.org/SGirousse/memory-rpg).

**Recommended** :

- [VS Code (latest)](https://code.visualstudio.com/)
- VS Code plugins :
  - [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) : A complete linter for Markdown (auto-format, problem detection, table of content, ...).
  - [Python Docstring Generator](https://marketplace.visualstudio.com/items?itemName=njpwerner.autodocstring) : Gain some time by updating docstring.
- Python library :
  - [virtualenv (lastest)](https://virtualenv.pypa.io/en/latest/) : Allows to create Python virtual environments and isolate your developments.
  - [HTML Preview](https://marketplace.visualstudio.com/items?itemName=tht13.html-preview-vscode) : Allows to preview HTML files in VS Code.

### Run the application

1. Start a bash ;
2. Move to the application code root ;
3. Execute commands to setup your repo :

    ```bash
    # Installing in root folder allows to open all repositories of the project in one VSCode windows at once : but that's totally developer choice :)
    python -m virtualenv ../.venv
    pip install -r requirements.txt -r dev-requirements.txt
    ```

4. Execute commands to start the application :

    ```bash
    uvicorn app.main:app --reload
    ```

    > `reload` option allows uvicorn to live load your code changes.

### Code checks

#### Manual checks

Code checks should be ran from the root of the repository.

>___
> 📝 **tl;dr : simply all commands**
> ___
> ```bash
> pylint --rcfile=.code_check_config/.pylintrc -f colorized app/
> coverage run --rcfile=.code_check_config/.coveragerc -m pytest app/tests/ --tb=line --durations=3
> coverage report -m
> bandit --ini .code_check_config/.banditrc -r
> ```
> ___

Used commands in the project are :

- **Unit tests** : `pytest app/tests`
  - Failures should be resolved ;
  - Warning should be resolved or documented (ticket or docstring).
  - Some arguments can be handful, check [pytest CLI tips and tricks](https://verdantfox.com/blog/view/9-pytest-tips-and-tricks-to-take-your-tests-to-the-next-level#1-useful-command-line-arguments) for more details.
- **Code quality** : `pylint --rcfile=.code_check_config/.pylintrc -f colorized app/`
  - Project threshold is `8.0` : if below, please improve code quality.
- **Code coverage** : 
    ```bash
    coverage run --rcfile=.code_check_config/.coveragerc -m pytest app/tests/ --tb=line --durations=3
    coverage report -m
    ```
  - It is expected to be the highest possible : see [code coverage guidelines](coding_guide.md#code-coverage) for details on how-to.
  - `coverage html --rcfile=.code_check_config/.coveragerc` will generate a html report.
- **Code security** : `bandit --ini .code_check_config/.banditrc -r`
  - Ensure there is no common security issue based a on static code analysis.
  - `mkdir -p .code_check_config/.banditcov && bandit --ini .code_check_config/.banditrc -r --format html > .code_check_config/.banditcov/index.html` will generate a html report.

>___
> 📝**Info**
> ___
> ⚙️Configuration of tools are stored under `.code_check_config` folder :
> - Coverage :&ensp; [.coveragerc](../../.code_check_config/.coveragerc)
> - Pytest :&emsp;&emsp; [.pylintrc](../../.code_check_config/.pylintrc)
> - Bandit :&emsp;&emsp; [.banditrc](../../.code_check_config/.banditrc)
> ___


#### Git hooks

Git hooks can be helpful to save you some time on some systematic actions and/or controls to do after/before git action. Check [Git hooks usage in the project](./hook_usage.md) to know more about it.

#### Pipelines

All checks are dones in pipelines at Merge Request (see gitflow [here](coding_guide.md)).

## VS Code Setup

You can use as you like, and even not being using it, but here are some recommendations.

### Recommended plugins

Install "recommended" plugins from [Pre-Requisites](#pre-requisites) section.

### Preferences Configuration

These are a snippet of my `User Settings` (used at global level), but you may store it as Workspace Settings` (used at local project) to avoid conflicts with other projects.  
See difference from VS Code [Get Started page](https://code.visualstudio.com/docs/getstarted/settings).

```json
{
    "git.enableSmartCommit": true,
    "python.venvPath": "${workspaceFolder}/.venv",
    "python.pythonPath": "${workspaceFolder}/.venv/Scripts/python.exe",
    "python.linting.pylintPath": "${workspaceFolder}/.venv/Scripts/pylint.exe",
    "python.linting.enabled": true,
    "python.linting.pylintUseMinimalCheckers": false,
    "python.linting.lintOnSave": true,
    "python.linting.pylintEnabled": true,
    "python.linting.pydocstyleEnabled": true,
    "python.linting.banditEnabled": false,
    "python.linting.pylintArgs": ["--max-line-length=120"],
    "python.formatting.provider": "autopep8",
    "python.formatting.autopep8Args": ["--max-line-length", "120", "--experimental"],
    "python.defaultInterpreterPath": "${workspaceFolder}/.venv/Scripts/python.exe",
    "editor.codeActionsOnSave": {
        "source.organizeImports": true,
    },
    "editor.rulers": [
        120
    ],
    "editor.formatOnSave": true,
    "terminal.integrated.defaultProfile.windows": "Debian (WSL)",
    "terminal.integrated.profiles.windows": {
        "PowerShell": {
            "source": "PowerShell",
            "icon": "terminal-powershell"
        },
        "Command Prompt": {
            "path": [
                "${env:windir}\\Sysnative\\cmd.exe",
                "${env:windir}\\System32\\cmd.exe"
            ],
            "args": [],
            "icon": "terminal-cmd"
        },
        "Debian (WSL)": {
            "source": "Git Bash"
        }
    }
}
```

## Code contribution

See the [Coding Guide](coding_guide.md) to have all informations about coding rules, gitflow, versioning, ... well anything I was not bored enough to write :-).

## More

### Known issues

#### Cannot open VS Code User Preferences as JSON

If only the UI is shown and not the JSON option, you may check [that link](https://stackoverflow.com/a/70584191/17853139).

#### FileNotFoundError while running pytests

**Problem** :

When running `pytest`, the below error is thrown :

```py
FileNotFoundError: [Errno 2] No such file or directory: 'app/filer/memories/mem004.html'
```

**Solution** :

You are most likely in the wrong folder and it does not get either the correct `.env` or `pytest.ini` file. From `backend` folder, run `pytest app/tests`.

### Resources

Below links are good resources regarding that project :

- [FastAPI documentation / tutorial](https://fastapi.tiangolo.com/tutorial/).

> ___
> ↩️ Browse back to [summary](./INDEX.md) page.