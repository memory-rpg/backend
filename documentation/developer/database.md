# Database model review

The database model is quite simple and is discussed below.

There are 2 separated schema :
- `Core` which will store "technical" data such as Users informations. Related to authentication.
- `Game` which will store "gameplay" related data.

There is no consideration such as isolating games content from each other as performances / table access is not (and should never be :-) ) a problematic.


## Database diagram

![Database diagram](.img/database_diagram.png)
>___
> 📝 **Info**
> ___
> You can contribute to [this diagram](https://lucid.app/lucidchart/a07c0f81-7675-4552-be83-b08aa700bea2/edit?invitationId=inv_c445088b-d3b6-4eb5-95b1-8b3d3f45fbd3) on Lucidchat 
> ___

- `Player` is the link between a user and its games. For now there no abstraction like "_A player could play several characters_" that may become a thing in future.
- `Game` is kind of key and central table which will have a real impact on [APIs design](../architecture/apis.md) as any element should be related to a `Game` (even if indirect).

> ___
> ↩️ Browse back to [summary](./INDEX.md) page.