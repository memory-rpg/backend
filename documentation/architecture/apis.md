# API design

API design choice are :
- Ensure all APIs needing to be authenticated.
  - Exceptions to previous rules :
    - `GET /` root page with basic app information.
    - `GET /docs` auto-generated swagger page.
    - `GET /favicon` return the app favicon.
    - `POST /token` allows user authentication by providing a valid token.
      >___
      > 📝 From a developer point of view, publics APIs should be defined in `main.py`.
      > ___
- APIs are following some grouping logic.  
  Grouping allows to apply some common rules (access control mainly) and some predictability in its usage from a consumer POV.
  - `/games` : All games related APIs (game list, players, memories, ...)
  - `/admins` : All admins related APIs.
  - `/users` : All users related API.
- When applicable, a route `<something>/me` returning the authenticated users informations. It is similar to `<something>/<current_user_id>`, simply convenient :)
- APIs must follows RESTful main conventions (regarding verbs, collection naming access and path vs query parameters logic)
- APIs can be taggued to allows categorization for consumers.


>___
> 💡 **Tips**
> ___
> APIs real implementation can be found publicly in the swagger at `/docs` URLs.
> ___

> ___
> ↩️ Browse back to [summary](./INDEX.md) page.
