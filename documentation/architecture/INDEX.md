# Security documentation

That section contains all documentation related to the high-level technical vision about flows, technology choices, etc.

It may happens that it does overlap with the [developer](../developer/INDEX.md) or [domain](../domain/INDEX.md) sections on some subjects. In that case, architecture documentation should reference the detailled implementation documentation instead of duplicating it.

## Current section documentation files

| File                                              | Description                                                  |
| :------------------------------------------------ | :----------------------------------------------------------- |
| [../](../INDEX.md)                                | Back to previous documentation level.                        |
| [INDEX.md](./INDEX.md)                            | Current file.                                                |
| [Application security](./application_security.md) | How security is being managed/addressed in the application ? |
| [APIs design](./apis.md)                          | What are guidelines for APIs design ?                        |



## Sub-sections documentation

>___
> This is the end of the section : go back to [previous section](../INDEX.md). 🔚
