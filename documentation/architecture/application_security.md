# Application security

On a general note, application security must be managed at all levels : the way we code, libraries we uses, how we store data, how the application is accessed, ... so it should be _by-design_ in our everyday methodology.

That said, our application is having some tool to help and implement the security. Here are some links to provide details specific security topics :

| Topic                                   | Description                                                                                  | Detailled section |
| :-------------------------------------- | :------------------------------------------------------------------------------------------- | :---------------- |
| **Database security**                   | How passwords are store ? How can the database be accessed ?                                 | TO BE LINKED      |
| **Application authentication**          | How can API can be accessed ? How permissions are managed ?                                  | TO BE LINKED      |
| **Static Application Security Testing** | Control on our code and used libraries : are they secure ?                                   | TO BE LINKED      |
| **Development best pratices**           | Simple good habits to avoid generating big holes behind all our beautiful protocols & tools. | TO BE LINKED      |

Some subjects are not (and may not due to the nature of the "fun-demo-learning" aspect of the project) be addressed, like :
- Web Application Firewall (WAF),
- Deployment target security (networking, secrets management, ...)
- PenTesting (see [MRPG-4](https://w31q1.atlassian.net/browse/MRPG-4)),
- All others I forgot :-)

## Table of content
- [Application security](#application-security)
  - [Table of content](#table-of-content)
  - [Security tools](#security-tools)
  - [Application access and usage](#application-access-and-usage)
    - [API Authentication](#api-authentication)
      - [JWT Token management](#jwt-token-management)
      - [User authentication](#user-authentication)
      - [Security exceptions](#security-exceptions)
    - [Data access restriction](#data-access-restriction)
  - [Database security](#database-security)
    - [Database server technology](#database-server-technology)
    - [Database access & permissions](#database-access--permissions)
    - [Database sensitive data mangement](#database-sensitive-data-mangement)

## Security tools

| Tool                      | Version  | Target                   | Usage in app                                              | Official doc                                                        |
| :------------------------ | :------- | :----------------------- | :-------------------------------------------------------- | :------------------------------------------------------------------ |
| Bandit                    | `latest` | SAST                     | [Coding guide](coding_guide.md#security)                  | [Read the docs](https://bandit.readthedocs.io/en/latest/index.html) |
| passlib[bcrypt]           | `latest` | Password encryption.     | [Password encryption](#database-sensitive-data-mangement) | [Read the docs](https://passlib.readthedocs.io/en/stable/)          |
| python-jose[cryptography] | `latest` | JWT token encode/decode. | [Token encryption](#api-authentication)                   | [Read the docs](https://python-jose.readthedocs.io/en/latest/)      |

## Application access and usage

Here are 2 topics :
1. Authentication to the application ;
2. Data access permissions.

The first topic will [be managed at the API level](#api-authentication) by using of `OAuth2` protocole. The second topic will be addressed at [the code level, see that section](#data-access-restriction) for details.

### API Authentication

For now, all endpoints outside `main.py` needs an authentication. Authentication is based on `OAuth2` protocol with simple User/Password logic. It follows most of [FastAPI security tutorial](https://fastapi.tiangolo.com/tutorial/security/) guidelines.

There are 2 majors principles :
1. Before starting to interact with the backend, user should generate a token using `POST /token` API. The caller should provide `username` and `password` informations (see [oauth official documentation](https://www.oauth.com/oauth2-servers/access-tokens/password-grant/)) and the application will validate it :
   1. If valid, a token if provided and can be used to call any API.
   2. If invalid, then a HTTP `401 Unauthorized` with the detail `{"detail":"Incorrect username or password"}` will be returned.
2. When calling an endpoint, the presence of a valid token is controlled.
   1. If valid, endpoint will be executed.
   2. If invalid, then a HTTP `401 Unauthorized` with the detail `{"detail":"Not authenticated"}` will be returned.

#### JWT Token management

JWT tokens are being generated, encoded and decoded based on secret key and an algorithm :
- The key **depends on the environment** where the application is running.
- The algorithm is `HS256`.

For now, the application will check the jwt secret key in the environment variables and it should be named `JWT_SECRET_KEY`. You can generate a secret key by running the command :
```bash
JWT_SECRET_KEY=$(openssl rand -hex 32)
```

Token is being built with an encoded content for a specific duration :

```py
def create_access_token(data: dict, expires_delay_in_min: Optional[int] = 90) -> str:
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=expires_delay_in_min)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, os.environ["JWT_SECRET_KEY"], algorithm=ALGORITHM)
    return encoded_jwt
```

- `data` is only the `username` (pushed from `main.py`.
- token validity is set at **90 minutes**.

#### User authentication

User is being identifier by user/password. The password must generated using `bscript` algorithm.

#### Security exceptions

Some APIs do not needs a JWT token, they must be tagged `PUBLIC` :
| Protocol | Endpoint    | Must provide valid credentials | Must have a valid token |                                                                                                                                                                         |
| :------- | :---------- | :----------------------------- | :---------------------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `GET`    | `/`         | ❌                              | ❌                       | Application informations (version, authors) we do not want to make private                                                                                              |
| `POST`   | `/`         | ✔️                              | ❌                       | Provide a token for other APIs                                                                                                                                          |
| `GET`    | `/favicon`  | ❌                              | ❌                       | Return the application favicon                                                                                                                                          |
| `GET`    | `/is-alive` | ❌                              | ❌                       | Allows external tools to check the server status (like an application gateway, kubernetes container check, cron-job to ensure you Heroku job does not go to sleep, etc. |
| `GET`    | `/docs`     | ❌                              | ❌                       | Swagger                                                                                                                                                                 |

A test is dedicated to the control this API tag.

### Data access restriction

>___
> 📝 **Info**
> ___
> By **user** we often refer at a **client** (like a frontend) which is managing the (real) user interactions with the API.

Once [connected with a token](#api-authentication), user can call any API of the application. Although, they may have access to restricted/filtered informations related to their role in the game.  
That whole subject is still be more documented.

>___
>❌🗋 **That section needs to be completed.**
>___


>___
> 📝 **Info**
> ___
> The application is not meant to store sensitive data, the permission management will be focused on the gameplay logic.

## Database security

>___
> ⚠️ **SQLite limitations**
> ___
> SQLite being used for now, there is no security around it. This should be addressed in [MRPG-33](https://w31q1.atlassian.net/browse/MRPG-33).  

### Database server technology

At the moment, SQLite is being used as database server due to the ease of deploying an application with that technology.

In future, SQLite should be only used for testing purposes (and possibly as a light-but-unsecure package).

### Database access & permissions

>___
> ⚠️ **No access restriction**
> ___
> Being run in local on a SQLite, the database access is not being restricted. This subject will be addressed at deployment time (depending on **how** and **where** we are going to deploy this).  

>___
> 📝 **SQLite password**
> ___
> In case SQLite is selected a possible database package, it must be [protected by a password](https://newbedev.com/password-protect-a-sqlite-db-is-it-possible) for a minimal protection.

>___
> ⚠️ **No permissions**
> ___
> SQLite does not provide any kind of permission management. See [SQLite "omitted"](https://sqlite.org/omitted.html).

### Database sensitive data mangement

Currently, sensitive data is for passwords only (cf. `Core.User.user_pwd`). There is no check, automation, etc. in the database itself so one could store _anything TEXT-like_ in the password field.

On the other hand, the application will expect them to be encrypted with `bcrypt` (here a [wikipedia link,](https://en.wikipedia.org/wiki/Bcrypt) yes, for real). It will systematically try to compare the hashed user input with the database content.

>___
> 📝 **User creation**
> ___
> There is no user creation module for now, so it should be manually done in the application.

you can generate a hashed password by running the following python code:

```py
from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
print(pwd_context.hash("_ch4ng3_M3_"))
print(pwd_context.hash("gueguest2022"))
```

> ___
> ↩️ Browse back to [summary](./INDEX.md) page.
