
# Memory mechanic review

The memory access by players through game is the main point and interest of that application. That page will describe the general mechanics associated with the memories access.

## Table of content
- [Memory mechanic review](#memory-mechanic-review)
  - [Table of content](#table-of-content)
  - [Memories in the RPG](#memories-in-the-rpg)
  - [Memory access](#memory-access)
    - [General flow diagram](#general-flow-diagram)
  - [Blurring calculation](#blurring-calculation)
    - [Blur factors review](#blur-factors-review)
      - [Clarity base value](#clarity-base-value)
      - [Mood color (malus)](#mood-color-malus)
      - [Player Status (malus)](#player-status-malus)
      - [Memory intensity (bonus)](#memory-intensity-bonus)
    - [Summary](#summary)
    - [Table of examples](#table-of-examples)

## Memories in the RPG

Base of that game of role is that players have lost some memories (can be some, can be all : that's up to Game Master). All along the game, players will meet people, discover places and items, ... and this will give them access to associated memories.

>___
> 📝 **Info**
> ___
> Peoples, items, places, ... will be named `element` in the following.
> ___

For each `element` a player can have _at most_ 1 associated memory. And several players can have an associated memory to it. When discovering it, they will have access to it with 3 technical rules :

1. The access is granted through the `element` code, so it must be shared by the GM.
2. Accessing a memory is never perfect, some pieces of it will be missing. The details on the blur factors are described in another section.
3. The access is unique : once accessed, a memory is being `frozen` so it will be always the same.

Nothing prevent the GM to choose to use some _perception_ ability or any other trick to choose if they grant access the access to the memory.

## Memory access

Technically speaking, accessing a memory can be achieved trough the `memories/` API. See swagger documentation for more information.

In game mecanism, it would be preferred to call the API `items/{item_id}/memories/me` to access the memory. Item ID will be provided by the GM.

>___
> ⚠️ **Warning**
> ___
> In future, accessing a item should be done with a random generated code (e.g `xV25y`) with an API like `items/{item_code}/memories/me` instead of the code preventing clumsy players so misstype and smartass players to try all possible `item_id` combinations.
> ___

The way the API is accessed is up to the frontend client.

### General flow diagram

![Blurring memory flow](.img/memory_activity_diagram.png)

Main steps are :

1. Check that user is authenticated
2. Check that user actually have access to game elements
3. Calculate the blur factor (detailled in [blurring calculation](#blurring-calculation) section)
4. Generate the blurred memory
   1. Stored in the filer
   2. Update the database
5. Returns the blurred content

## Blurring calculation

A memory is basically a HTML text with 3 kinds of words :
- ones which **could** be blurred;
- ones which **must** be blurred : identified by a `<del>` tag ;
- ones which **must NOT** be blurred : identified by a `<mark>` tag.

For each word, we attempt to blur it with a certain random value (between 0 and 100). If it is higher than the **clarity value** (also between 0 and 100) then word letters will be masked/blurred (e.g. `Hello darkness my old friend` could become something like `Hello ████████ my ███ friend`).

Clarity value is being calculated based on various gameplay factors and some are simply activable/desactivable. The general formula is : `clarity_base_value * (1 - mood_reduction/100) * (1 - player_status_reduction) * (1 + memory_intensity)`.

All these variables are described in next section.

### Blur factors review

#### Clarity base value

`clarity_base_value` is a constant value of `50`. As `memory_intensity` can double the base value we have to divide it by half in the first place. Voilà. 


#### Mood color (malus)

The memory can be associated with a color which has been thinked as a color which translate a mood. The player can then select a color and its distance with the memory color will be calculated with the following steps : 
1. Compare hexadecimal values (in percentage of proximity) for red, green and blue ;
2. Calculate average of their summed difference to get a percentage.
3. Take 1/3 of that percentage as a reduction factor (1/3 because this is actually quite random and we do not want it to impact too much the final result).

>___
> 💡 **Tips**
> ___
> To activate that parameter, `Game.is_mood_color_impacting` must be set to `true`.
> ___

The mood can be a fun feature as it allows to :
- Create some small enigma (depending on the object, the aspect and the location, what is the most expect mood ?)
- Create some surpise and tips about the character past mindset (A destroyed decoration on the wall could be linked to `liberation` or `wrath` or maybe `joy`)
- To create some rewarding logic once the player will have understand better its characted past mindset.

But we recommend to share with players a table of moods vs colors to ensure everyone have the same definition (you can find many in Google)

>___
> ⚠️ **Ignored by default**
> ___
> If `Game.is_mood_color_impacting` is set to `true` but the API is called without a mood query parameter, then there will be a log warning and the mood factor will be ignored.
> ___

#### Player Status (malus)

The game is thinked with very simple logic where must of the "player statistics" stuff should be managed the old way. We here offer a way to select the player status (either physically or mentally) to `GOOD`, `INJURED`, `BAD` which will reduce the ability of remember on an exponential scale : `0.0`, `0.3` and `0.8`.

>___
> 💡 **Tips**
> ___
> To activate that parameter, `Game.is_player_status_impacting` must be set to `true`.
> ___

#### Memory intensity (bonus)

This is associated to the memory by the GM and is a value between [0;100]. The idea is that you will not easily forget intense memories (good or bad) no matter what.

Note that _intense_ is different from _important_ (both regarding the character and the whole story) ;-).

### Summary

| Factors                | Range   | Type       | Explanation                                                                                                                                      |
| :--------------------- | :------ | :--------- | :----------------------------------------------------------------------------------------------------------------------------------------------- |
| Clarity base value     | 50      | Flat value | A flat arbitrary static value.                                                                                                                   |
| Mood (color) reduction | [0;33]  | Percentage | Third of difference between memory color (selected by GM) and player mood color (selected in-game). See [mood color](#mood-color-malus) section. |
| Player status          | [0;80]  | Percentage |                                                                                                                                                  | Factor associated to the player physical/mental state. See [player status](#player-status-malus) section. |
| Memory intensity bonus | [0;100] | Percentage | Factor associated to the memory by the GM to improve the memory "clarity". See [memory intensity](#memory-intensity-bonus) section.              |

### Table of examples

| Operation \ Case               |     01 - best |   02 - worst | 03 - average |
| :----------------------------- | ------------: | -----------: | -----------: |
| Clarity value - init           |          50,0 |         50,0 |         50,0 |
| [**-**] Mood difference malus  |      (%) 00,0 |     (%) 33,0 |     (%) 16,0 |
| Clarity value - v2             |  50,0 -> 50,0 | 50,0 -> 33,5 | 50,0 -> 42,0 |
| [**-**] Player status malus    |      (%) 00,0 |     (%) 80,0 |     (%) 30,0 |
| Clarity value - v3             |  50,0 -> 50,0 |  33,5 -> 6,7 | 42,0 -> 29,4 |
| [**+**] Memory intensity bonus |     (%) 100,0 |      (%) 0,0 |     (%) 50,0 |
| Clarity value - v4             | 50,0 -> 100,0 |   6,7 -> 6,7 | 29,4 -> 44,1 |
| **Blur factor**                |    **00,0 %** |   **93,3 %** |   **55,9 %** |

If all other criteria are **perfect** but :

- Player status is **near death** their the blur factor will be 80 %.
- Mood color selection is **totally off/opposite** the blur factor will be 33 %.
- Memory intensity is **minimum** the blur factor will be 50 %.

> ___
> ↩️ Browse back to [summary](./INDEX.md) page.