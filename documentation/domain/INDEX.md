# Domain documentation

That section focus on subjects like :
- How that complex algorithm has been thinked/implemented ? With high level vision, important tips, etc.
- Describing and explaining not common, complex, ... code logic in some part of the application.
- Explaining (important) technical choices (like : comparing it with other natural choices).

Basically : if you asked yourself _Why ?_ and was not able to answer it in less than 30 minutes while using 🧠 then ... it should be documented !

## Current section documentation files

| File                                     | Description                                                                          |
| :--------------------------------------- | :----------------------------------------------------------------------------------- |
| [../](../INDEX.md)                       | Back to previous documentation level.                                                |
| [INDEX.md](./INDEX.md)                   | Current file.                                                                        |
| [Memory mechanic](./memory_mechanic.md)  | Describe how memories are managed in the application as the "main gameplay" feature. |
| [Decorators usage](./decorator_usage.md) | Describe usage of python decorators in the project.                                  |


## Sub-sections documentation

>___
> This is the end of the section : go back to [previous section](../INDEX.md). 🔚
