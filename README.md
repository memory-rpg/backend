# Memory RPG

This project is, above all, a self-learning project where libraries, coding practices, ... will be tested. It may or may not end on being something usable. So if you are wondering why there is some pen-test on an unused project ? Well ... that's the actual point for now ♥

That being said, the project should be an helper for a role-playing game where main gameplay is based on the fact that characters has lost their memories : players will then discover fragment along their RP sessions.

Issues are being managed with [Jira](https://w31q1.atlassian.net/jira/software/c/projects/MRPG/boards/1).

- [Memory RPG](#memory-rpg)
  - [Quick startup](#quick-startup)
    - [Pre-requisites](#pre-requisites)
    - [Run the application](#run-the-application)
    - [Project documentation & contribution](#project-documentation--contribution)

## Quick startup

>___
> 📝 **Info**
> ___
> This project has been developed on VS Code, on Windows, using a virtual environment and commands are bash.
> ___

### Pre-requisites

Before starting one may have :

- Installed [Python 3.10](https://www.python.org/downloads/release/python-3100/).
- Cloned the [memory-rpg repository](https://framagit.org/SGirousse/memory-rpg).

### Run the application

For a quick start :

1. Start a bash ;
2. Move to the application code root ;
    >___
    > 💡 It is recommended to create a virtual environment at that point.
    > ___
    > 
3. Execute commands

    ```bash
    pip install -r requirements.txt
    uvicorn app.main:app
    ```

You can now access the application :

- [Root page](http://localhost:8000/)
- [Swagger](http://localhost:8000/docs)

### Project documentation & contribution

- [Main documentation page](documentation/INDEX.md).
- Develop & tests :
  - Everything is detailed in the [Developer guide](documentation/developer/developer_guide.md).
- Contribute
  - [Contribute to code](documentation/developer/coding_guide.md).
  - [Contribute to documentation](documentation/CONTRIBUTE.md).