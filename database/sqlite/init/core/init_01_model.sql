--
-- Core schema initialisation script
-- This schema store all very technical informations about users.
--
-- An application user
CREATE TABLE IF NOT EXISTS Core.User (
    id INT PRIMARY KEY,
    user_name CHAR,
    user_pwd CHAR
);
