--
-- Game schema initialisation script
-- This schema store all game related informations.
--

-- A RPG game : this is a very central object for filtering.
CREATE TABLE IF NOT EXISTS Game.Game (
    id INT PRIMARY KEY,
    name CHAR,
    start_date_timestamp INT,
    end_date_timestamp INT,
    -- [0|1] If true, Game.Player.player_status has an impact on blur
    is_player_status_impacting INT,
    -- [0|1] If true, Game.Memory.mood_color will be used
    is_mood_color_impacting INT
);

-- A player is a character for a human player
CREATE TABLE IF NOT EXISTS Game.Player (
    id INT PRIMARY KEY,
    character_name CHAR UNIQUE,
    -- [NPC|PC|GM] (default : NPC)
    player_type CHAR,
    -- [GOOD|INJURED|BAD] (default : GOOD)
    player_status CHAR,
    user_id iNT,
    game_id INT
);

-- An item can be anything (item, landscape, NPC, ...)
CREATE TABLE IF NOT EXISTS Game.Item (
    id INT PRIMARY KEY,
    code CHAR UNIQUE,
    name CHAR UNIQUE,
    description CHAR,
    game_id INT
);

-- A memory is a file linked to an Item and a Player
CREATE TABLE IF NOT EXISTS Game.Memory (
    id INT PRIMARY KEY,
    -- Path to the base memory file written by GM
    base_file_path CHAR UNIQUE,
    -- Path to the memory file once blurred     
    blurred_file_path CHAR UNIQUE,
    -- Date when the memory has been accessed/blurred
    blurred_timestamp INT,
    -- [0; 100] Will reduce the blur effect     
    intensity INT,
    -- Color stored as hexadecimal value : can be compared to player selected color : Will reduce the blur effect       
    mood_color CHAR,
    item_id INT,
    player_id INT
);